<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/mpdf/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Api_tour extends CI_Model {

    function __construct() {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getTour($kategori)
    {
        $result =array();
        $get = $this->db->query("SELECT tr.id_tour,tr.id_destinasi,tr.id_durasi,tr.nama_tour,tr.harga_tour,tr.info_harga,tr.tanggal_tour, tr.url_preview_image as foto_tour, tdn.nama_destinasi, tk.nama_kategori, td.durasi FROM tour tr
      left join tour_destinasi tdn on tdn.id_destinasi=tr.id_destinasi
      left join tour_durasi td on td.id_durasi=tr.id_durasi
      left join tour_kategori tk on tk.id_kategori=tr.id_kategori WHERE tk.id_kategori=? ORDER BY tr.tanggal_tour DESC", array($kategori));
        $i=0;
        if ($get->num_rows()>0) {
            foreach ($get->result() as $rows) {
                $result[$i]['id_tour'] = $rows->id_tour;
                $result[$i]['id_destinasi'] = $rows->id_destinasi;
                $result[$i]['id_durasi'] = $rows->id_durasi;
                $result[$i]['nama_tour'] = $rows->nama_tour;
                $result[$i]['harga_tour'] = $rows->harga_tour;
                $result[$i]['info_harga'] = $rows->info_harga;
                $result[$i]['tanggal_tour'] = $rows->tanggal_tour;
                if ($rows->foto_tour!='' || $rows->foto_tour!=NULL) {
                    $result[$i]['foto_tour'] = base_url().$rows->foto_tour;
                }else {
                    $result[$i]['foto_tour'] = base_url('assets/img/coming-soon.png');
                }
                $result[$i]['nama_destinasi'] = $rows->nama_destinasi;
                $result[$i]['nama_kategori'] = $rows->nama_kategori;
                $result[$i]['durasi'] = $rows->durasi;
                $i++;
            }
            return [
                'status'=>'success',
                'message'=>'data tour ditemukan',
                'data'=>$result];
        }else {
            return [
                'status'=>'failed',
                'message'=>'Data tour belum diisi',
                'data'=>''];
        }
    }

    public function detailTour($id_tour)
    {
        $result = array();
        $data_foto = array();
        $data_harga = array();

        $get = $this->db->query("SELECT REPLACE(tr.id_tour,'-','') as id_tour,tr.nama_tour,tr.is_multiple_price,tr.harga_tour,tr.info_harga,tr.tanggal_tour,tr.itinerary,tr.persyaratan,tr.using_badge, tr.is_flight_include, tr.url_preview_image , tdn.nama_destinasi, tk.nama_kategori, td.durasi FROM tour tr
      left join tour_destinasi tdn on tdn.id_destinasi=tr.id_destinasi
      left join tour_durasi td on td.id_durasi=tr.id_durasi
      left join tour_kategori tk on tk.id_kategori=tr.id_kategori WHERE REPLACE(tr.id_tour,'-','')=?", array($id_tour));

        if ($get->num_rows()>0) {

            $rs = $get->row();
            $result['id_tour'] = $rs->id_tour;
            $result['nama_tour'] = $rs->nama_tour;
            $result['is_multiple_price'] = $rs->is_multiple_price;
            $result['harga_tour'] = $rs->harga_tour;
            $result['info_harga'] = $rs->info_harga;
            $result['tanggal_tour'] = $rs->tanggal_tour;
            $result['itinerary'] = $rs->itinerary;
            $result['persyaratan'] = $rs->persyaratan;
            $result['using_badge'] = $rs->using_badge;
            $result['is_flight_include'] = $rs->is_flight_include;
            if ($rs->url_preview_image!='' || $rs->url_preview_image!=NULL) {
                $result['url_preview_image'] = base_url().$rs->url_preview_image;
            }else {
                $result['url_preview_image'] = base_url('assets/img/coming-soon.png');
            }
            $result['nama_destinasi'] = $rs->nama_destinasi;
            $result['nama_kategori'] = $rs->nama_kategori;
            $result['durasi'] = $rs->durasi;

            $getFoto = $this->db->query("SELECT REPLACE(id_image,'-','') as id_image, url_image from tour_image where REPLACE(id_tour, '-','')=?", array($result['id_tour']));

            $i=0;
            if ($getFoto->num_rows()>0) {

                foreach ($getFoto->result() as $rows) {
                    $data_foto[$i]['id_image'] = $rows->id_image;
                    if ($rows->url_image!='' || $rows->url_image!=NULL) {

                        $data_foto[$i]['url_image'] = base_url().$rows->url_image;
                    }else {
                        $data_foto[$i]['url_image'] = base_url('assets/img/coming-soon.png');
                    }
                 $i++;
                }
            }else {
                $data_foto = array();
            }


            $get_harga = $this->db->query("SELECT REPLACE(id_tour,'-','') as id_tour,price,text_price from tour_price where REPLACE(id_tour, '-','')=?", array($result['id_tour']));

            $j=0;
            if ($get_harga->num_rows()>0) {

                foreach ($get_harga->result() as $rows) {
                    $data_harga[$j]['id_tour'] = $rows->id_tour;
                    $data_harga[$j]['price'] = $rows->price;
                    $data_harga[$j]['text_price'] = $rows->text_price;
                 $j++;
                }
            }else {
                $data_harga[0]['id_tour'] = $result['id_tour'];
                $data_harga[0]['price'] = $result['harga_tour'];
                $data_harga[0]['text_price'] = 'Single';
            }
            $result['foto_tour'] = $data_foto;
            $result['tour_price'] = $data_harga;

            return [
                'status'=>'success',
                'message'=>'data tour ditemukan',
                'data'=>$result];

        }else {
            return [
                'status'=>'failed',
                'message'=>'Data tour tidak ditemukan',
                'data'=>''];
        }
    }

    public function getFilter($jenis)
    {
        $result  = array();

        if (!empty($jenis)) {

            $get = $this->db->query("SELECT * FROM tour_destinasi where id_kategori=?", array($jenis));

            if ($get->num_rows()>0) {

                $i=0;
                foreach ($get->result() as $rows) {
                    $result[$i]['id_destinasi'] = $rows->id_destinasi;
                    $result[$i]['nama_destinasi'] = $rows->nama_destinasi;
                    $result[$i]['deskripsi'] = $rows->deskripsi;
                    $i++;
                }

                return [
                    'status'=>'success',
                    'message'=>'Data tour ditemukan',
                    'data'=>$result];
            }else {

                return [
                    'status'=>'failed',
                    'message'=>'Data tour tidak ditemukan',
                    'data'=>''];
            }
        }else {

            return [
                'status'=>'failed',
                'message'=>'Parameter tidak boleh kosong',
                'data'=>''];
        }
    }

    public function getDurasi()
    {
        $result  = array();


        $get = $this->db->query("SELECT * FROM tour_durasi");

        if ($get->num_rows()>0) {

            $i=0;
            foreach ($get->result() as $rows) {
                $result[$i]['id_durasi'] = $rows->id_durasi;
                $result[$i]['durasi'] = $rows->durasi;
                $i++;
            }

            return [
                'status'=>'success',
                'message'=>'Data tour ditemukan',
                'data'=>$result];
        }else {

            return [
                'status'=>'failed',
                'message'=>'Data tour tidak ditemukan',
                'data'=>''];
        }
    }

    public function konfirmasi_pesanan($data)
    {

        $this->db->trans_begin();

            $dtpeserta = count($data['peserta']);
            $dt_harga_total=0;
            for ($i = 0; $i < $dtpeserta; $i++) {
              $dt_harga_total += $data['peserta'][$i]['harga'];
            }

              $startDate = time();
              $expired= date('Y-m-d H:i:s', strtotime('+3 day', $startDate));

              $kode_pembayaran = auto_code('INV-'.date('ym'),'');
              $kode_pemesanan = auto_code('RSO-'.date('ymd'),'');

            $register = array(
          'id_pemesanan'        =>getUUID(),
          'id_customer'        =>$data['id_customer'],
          'created_by'         =>$data['id_user'],
          'fk_tour'   =>$data['fk_tour'],
          'tanggal_jatuh_tempo_dp'  =>$expired,
          'total_pembayaran'   =>$dt_harga_total,
          'status_pembayaran'  =>'PENDING',
          'bank'  =>$data['bank'],
          'kode_pembayaran'    =>$kode_pembayaran,
          'kode_pemesanan'     =>$kode_pemesanan,
        );
        $this->db->set($register);
        $this->db->insert('tour_pemesanan', $register);

        $peserta = count($data['peserta']);
        for ($i = 0; $i < $peserta; $i++) {
          $dt_peserta[$i]['id_peserta'] = getUUID();
          $dt_peserta[$i]['fk_pemesanan_tour'] = $register['id_pemesanan'];
          $dt_peserta[$i]['nama_lengkap'] = $data['peserta'][$i]['nama_lengkap'];
          $dt_peserta[$i]['kategori'] = $data['peserta'][$i]['kategori'];
          $dt_peserta[$i]['harga'] = $data['peserta'][$i]['harga'];
          $dt_peserta[$i]['created_by'] = $data['id_user'];
          $this->db->insert('tour_pemesanan_peserta',$dt_peserta[$i]);
        }

        $data_invoice = $this->detail_pesanan($register['id_pemesanan']);
        $invoices['pemesanan'] = $data_invoice['data'];

        $invoice_file = str_replace("-", "", $kode_pemesanan);
        $mpdf = new \Mpdf\Mpdf(['format' => 'A4']);
        $mpdf->useSubstitutions = false;
        $mpdf->simpleTables = true;
        $mpdf->AddPage('P'); // Adds a new page in Landscape orientationw
        $mpdf->WriteHTML($this->load->view('konfirmasi_pemesanan_tour',$invoices, true));
        $mpdf->Output(FCPATH.'files/tour_invoices/'.$invoice_file.'.pdf','F');

        $this->db->where("id_pemesanan", $register['id_pemesanan']);
        $this->db->update('tour_pemesanan', array('url_invoice'=>'files/tour_invoices/'.$invoice_file.'.pdf'));


        if ($this->db->trans_status()===false) {
            $this->db->rollback();
            return ['status'=>'failed','message'=>'data tidak bisa diproses.','data'=>'0'];
        }else {
            $this->db->trans_commit();

            $mail = new PHPMailer();

            $invoices['link'] = '/files/tour_invoices/'.$invoice_file.'.pdf';

            $html = $this->load->view('kirim_invoice_tour',$invoices, true);

            $mail->IsHTML(true);    // set email format to HTML
            $mail->IsSMTP();   // we are going to use SMTP
            $mail->SMTPAuth   = true; // enabled SMTP authentication
            $mail->SMTPSecure = "ssl"; // prefix for secure protocol to connect to the server
            // $mail->SMTPDebug  = 2;
            $mail->Host       = "smtp.gmail.com"; // setting GMail as our SMTP server
            $mail->Port       = 465;   // SMTP port to connect to GMail
            $mail->Username   = "cyber.rosana@gmail.com";  // alamat email kamu
            $mail->Password   = "www.rosanatourtravel.com";            // password GMail
            $mail->SetFrom("cyber.rosana@gmail.com", 'Penting !');  //Siapa yg mengirim email
            $mail->Subject    = 'Invoice pesanan anda';
            $mail->Body       = $html;
            $mail->AddAddress($data_invoice['data']['email']);
            $mail->send();
            return ['status'=>'ok','message'=>'data pemesanan berhasil diproses.','data'=>'1'];
        }
        $this->db->trans_complete();
    }

    public function pemesanan_customer($id_customer)
    {
        $pesanan = $this->db->query("SELECT id_pemesanan,nama,nik,email, kode_pemesanan,status_pembayaran,tanggal_jatuh_tempo_dp, total_pembayaran,nama_tour, nama_destinasi
            from tour_pemesanan
            left join customer on REPLACE(customer.id_customer,'-','')=REPLACE(tour_pemesanan.id_customer,'-','')
            left join tour on REPLACE(tour.id_tour,'-','')=REPLACE(tour_pemesanan.fk_tour,'-','')
            left join tour_destinasi on REPLACE(tour_destinasi.id_destinasi,'-','')=REPLACE(tour.id_destinasi,'-','')
            where REPLACE(customer.id_customer,'-','')=?",array(str_replace("-", "", $id_customer)));
        if ($pesanan->num_rows()!=0) {
            $i=0;
            foreach ($pesanan->result() as $k) {

                $result[$i]['id_pemesanan'] = $k->id_pemesanan;
                $result[$i]['nama'] = $k->nama;
                $result[$i]['nik'] = $k->nik;
                $result[$i]['email'] = $k->email;
                $result[$i]['kode_pemesanan'] = $k->kode_pemesanan;
                $result[$i]['nama_tour'] = $k->nama_tour;
                $result[$i]['tanggal_jatuh_tempo_dp'] = $k->tanggal_jatuh_tempo_dp;
                $result[$i]['total_pembayaran'] = $k->total_pembayaran;
                $result[$i]['status_pembayaran'] = $k->status_pembayaran;

                $peserta = $this->db->query("SELECT nama_lengkap,kategori, harga  from tour_pemesanan_peserta where fk_pemesanan_tour=?", array($result[$i]['id_pemesanan']));
                $dt_peserta = array();
                $j=0;
                foreach ($peserta->result() as $key => $r) {
                    $dt_peserta[$j]['nama_lengkap'] = $r->nama_lengkap;
                    $dt_peserta[$j]['kategori'] = $r->kategori;
                    $dt_peserta[$j]['harga'] = $r->harga;
                    $j++;
                }
                $result[$i]['peserta'] = $dt_peserta;
                $i++;
            }
            return ['status'=>'ok','message'=>'data ditemukan.','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'data tidak ditemukan.','data'=>'0'];
        }
    }

    public function detail_pesanan($id)
    {
       $pesanan = $this->db->query("SELECT id_pemesanan,nama,nik,email, nomor_hp,kode_customer,kode_pemesanan,tanggal_jatuh_tempo_dp, total_pembayaran,nama_tour, tanggal_tour, nama_destinasi
        from tour_pemesanan
        left join customer on REPLACE(customer.id_customer,'-','')=REPLACE(tour_pemesanan.id_customer,'-','')
        left join tour on REPLACE(tour.id_tour,'-','')=REPLACE(tour_pemesanan.fk_tour,'-','')
        left join tour_destinasi on REPLACE(tour_destinasi.id_destinasi,'-','')=REPLACE(tour.id_destinasi,'-','')
        where REPLACE(id_pemesanan,'-','')=?",array(str_replace("-", "", $id)));
        if ($pesanan->num_rows()!=0) {
            $psn = $pesanan->row();
            $result['id_pemesanan'] = $psn->id_pemesanan;
            $result['nama'] = $psn->nama;
            $result['nik'] = $psn->nik;
            $result['email'] = $psn->email;
            $result['nomor_hp'] = $psn->nomor_hp;
            $result['kode_customer'] = $psn->kode_customer;
            $result['kode_pemesanan'] = $psn->kode_pemesanan;
            $result['nama_tour'] = $psn->nama_tour;
            $result['tanggal_jatuh_tempo_dp'] = $psn->tanggal_jatuh_tempo_dp;
            $result['total_pembayaran'] = $psn->total_pembayaran;
            $result['tanggal_tour'] = $psn->tanggal_tour;
            $result['nama_destinasi'] = $psn->nama_destinasi;

            $peserta = $this->db->query("SELECT nama_lengkap,jenis_identitas, nomor_identitas,id_peserta,kategori, harga  from tour_pemesanan_peserta where fk_pemesanan_tour=?", array($result['id_pemesanan']));
            $dt_peserta = array();
            $j=0;
            foreach ($peserta->result() as $key => $r) {
                $dt_peserta[$j]['id_peserta'] = str_replace("-", "", $r->id_peserta);
                $dt_peserta[$j]['nama_lengkap'] = $r->nama_lengkap;
                $dt_peserta[$j]['kategori'] = $r->kategori;
                $dt_peserta[$j]['harga'] = $r->harga;
                $dt_peserta[$j]['jenis_identitas'] = $r->jenis_identitas;
                $dt_peserta[$j]['nomor_identitas'] = $r->nomor_identitas;
                $j++;
            }
            $result['peserta'] = $dt_peserta;
            return ['status'=>'ok','message'=>'data ditemukan.','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'data tidak ditemukan.','data'=>'0'];
        }
    }

    public function editPeserta($id_peserta)
    {
        $get = $this->db->query("SELECT * FROM tour_pemesanan_peserta where replace(id_peserta,'-','')=?", array(str_replace("-","",$id_peserta)));
        if ($get->num_rows()!=0) {
            $result = $get->row();

            $result->id_peserta = str_replace("-", "", $result->id_peserta);

            unset($result->status_data,$result->is_active,$result->created_by,
                $result->created_at,$result->last_modified,
                $result->id_referensi_pegawai,$result->kategori,$result->harga);

            return ['status'=>'ok','message'=>'data ditemukan.','data'=>$result];

        }else {
            return ['status'=>'failed','message'=>'data tidak ditemukan.','data'=>array()];
        }
    }

    public function updatePeserta($id_peserta,$data)
    {
        $this->db->where('replace(id_peserta,"-","")', str_replace("-", "", $id_peserta));
        $this->db->update('tour_pemesanan_peserta', $data);
        if ($this->db->affected_rows()) {
            return ['status'=>'success','message'=>'Data Peserta pemesanan tour berhasil diupdate.','data'=>'1'];
        }else {
            return ['status'=>'failed','message'=>'Data Peserta pemesanan tour gagal diupdate','data'=>'0'];

        }
    }

}
