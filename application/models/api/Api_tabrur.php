<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Api_tabrur extends CI_Model {

    function __construct() {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
        date_default_timezone_set('Asia/Jakarta');
    }

    function registrasiTabrur($data) {

        $customer['nik'] = $data['nik'];
        $customer['nama'] = $data['nama'];
        $customer['nama_ayah'] = $data['nama_ayah'];
        $customer['tempat_lahir'] = $data['tempat_lahir'];
        $customer['tanggal_lahir'] = $data['tanggal_lahir'];
        $customer['jenis_kelamin'] = $data['jenis_kelamin'];
        $customer['status'] = $data['status'];
        $customer['nomor_hp'] = $data['nomor_hp'];
        $customer['email'] = $data['email'];
        $customer['pendidikan'] = $data['pendidikan'];
        $customer['pekerjaan'] = $data['pekerjaan'];
        $customer['penghasilan'] = $data['penghasilan'];
        $customer['id_provinsi'] = $data['id_provinsi'];
        $customer['id_kabupaten'] = $data['id_kabupaten'];
        $customer['alamat'] = $data['alamat'];
        $customer['url_ktp'] = $data['url_ktp'];
        $customer['is_tab_umrah'] = '1';

        $this->db->set($customer);
        $this->db->trans_begin();
        $this->db->where("fk_id_user", $data['fk_id_user']);
        $this->db->update('customer', $customer);

        $cekTabungan = $this->getDataTabrur($data['id_customer']);

        if ($cekTabungan!=0) {

            $updateTabrur['syarat_ketentuan'] = $data['syarat_ketentuan'];
            $updateTabrur['register_by'] = $data['fk_id_user'];
            $updateTabrur['syarat_ketentuan'] ='01';
            $this->db->where("REPLACE(id_customer,'-','')", str_replace("-", "", $data['id_customer']));
            $this->db->update('tabura', $updateTabrur);

        }else {

            $tabrur['id_tabura'] = getUUID();
            $tabrur['id_customer'] = $data['id_customer'];
            $tabrur['kode_reg'] = $data['kode_reg'];
            $tabrur['kode_pembayaran'] = $data['kode_pembayaran'];
            $tabrur['syarat_ketentuan'] = $data['syarat_ketentuan'];
            $tabrur['jumlah_jamaah'] = $data['jumlah_jamaah'];
            $tabrur['target_saldo'] = $data['target_saldo'];
            $tabrur['sisa_target_saldo'] = $data['sisa_target_saldo'];
            $tabrur['setoran_awal'] = $data['setoran_awal'];
            $tabrur['sisa_setoran_awal'] = $data['sisa_setoran_awal'];
            $tabrur['setoran_perbulan'] = $data['setoran_perbulan'];
            $tabrur['register_by'] = $data['fk_id_user'];
            $tabrur['status_tabungan'] = '01';
            $tabrur['deadline_setoran_awal'] = $data['deadline_setoran_awal'];

            $this->db->insert('tabura', $tabrur);
        }

        if ($this->db->trans_status()===false) {

            $this->db->trans_rollback();
            return [
                'status'=>'failed',
                'message'=>'Pendftaran tabungan umrah gagal, silahkan cek kembali data anda',
                'data'=>''];
        }else {

            $this->db->trans_commit();
            return [
                'status'=>'success',
                'message'=>'Pendaftaran tabungan umrah berhasil dilakukan',
                'data'=>array('url_ktp'=>$data['url_ktp'])];
        }
        $this->db->trans_complete();
    }

    public function getCustomer($id)
    {
        $this->db->select("*");
        $this->db->from('customer');
        $this->db->where("fk_id_user", $id);
        $result = $this->db->get();
        return $result->row();
    }

    public function getDataTabrur($id_customer)
    {
        $this->db->select("*");
        $this->db->from("tabura");
        $this->db->where("REPLACE(id_customer,'-','')", str_replace("-", "", $id_customer));
        return $this->db->count_all_results();
    }

    public function getTabunganCustomer($data)
    {
        $dataTabungan = array();
        $histori = array();
        if ($data['fk_id_user']!="") {

            $customer = $this->getCustomer($data['fk_id_user']);

            $get = $this->db->query("SELECT id_tabura,nomer_va, kode_reg,status_tabungan, target_saldo, sisa_target_saldo, setoran_awal,setoran_perbulan, kode_pembayaran, sisa_setoran_awal,deadline_setoran_awal from tabura where id_customer=?", array($customer->id_customer));

            if ($get->num_rows()>0) {

                $row = $get->row();

                $dataTabungan['id_tabura'] = $row->id_tabura;
                $dataTabungan['nomer_va'] = $row->nomer_va;
                $dataTabungan['kode_reg'] = $row->kode_reg;
                $dataTabungan['kode_pembayaran'] = $row->kode_pembayaran;
                $dataTabungan['target_saldo'] = $row->target_saldo;
                $dataTabungan['sisa_target_saldo'] = $row->sisa_target_saldo;
                $dataTabungan['setoran_awal'] = $row->setoran_awal;
                $dataTabungan['sisa_setoran_awal'] = $row->sisa_setoran_awal;
                $dataTabungan['setoran_perbulan'] = $row->setoran_perbulan;
                $dataTabungan['kode_pembayaran'] = $row->kode_pembayaran;
                $dataTabungan['deadline_setoran_awal'] = $row->deadline_setoran_awal;

                  if ($row->status_tabungan=='01') {
                    $dataTabungan['status_text'] = 'Mendaftar';
                  }else if($row->status_tabungan=='02'){
                    $dataTabungan['status_text'] = 'Proses Setoran Awal';
                  }else if($row->status_tabungan=='03'){
                    $dataTabungan['status_text'] = 'Proses Menabung';
                  }else if($row->status_tabungan=='04'){
                    $dataTabungan['status_text'] = 'Tabungan Lunas';
                  }else if($row->status_tabungan=='05'){
                    $dataTabungan['status_text'] = 'Proses Menabung Selesai';
                  }
                  $dataTabungan['nama']  = $customer->nama;

                  $getSetoranAwal = $this->db->query("SELECT tba.tanggal_pembayaran, tba.nominal, tba.mekanisme, tba.catatan, 'setoran' as type FROM tabura_setoran_awal tba
                        WHERE tba.id_tabura=?
                        UNION
                        SELECT tst.tanggal_pembayaran, tst.nominal, tst.mekanisme, tst.catatan, 'tabungan' as type FROM tabura_setoran_tabungan tst
                        WHERE tst.id_tabura=? order by tanggal_pembayaran desc", array($row->id_tabura,$row->id_tabura));
                  if ($getSetoranAwal->num_rows()>0) {

                      $i=0;
                      foreach ($getSetoranAwal->result() as $rows) {
                          $histori[$i]['tanggal_pembayaran'] = $rows->tanggal_pembayaran;
                          $histori[$i]['nominal'] = $rows->nominal;
                          $histori[$i]['mekanisme'] = $rows->mekanisme;
                          $histori[$i]['catatan'] = $rows->catatan;
                          $histori[$i]['type'] = $rows->type;
                          $i++;
                      }
                  }else {
                      $histori = array();
                  }

                  $dataTabungan['history'] = $histori;

                  return [
                    'status'=>'success',
                    'message'=>'Data tabungan umroh berhasil ditemukan',
                    'data'=>$dataTabungan];

            }else {
                return [
                    'status'=>'failed',
                    'message'=>'Data tabungan umrah tidak ditemukan',
                    'data'=>''];
            }
        }else {
            return [
                'status'=>'failed',
                'message'=>'Data parameter tidak boleh kosong',
                'data'=>''];
        }
    }

    public function setoranAwal($data)
    {
        $get = $this->db->query("SELECT * from tabura where id_tabura=?", $data['id_tabura']);

        if ($get->num_rows()>0) {

            $row = $get->row();

            if ($data['nominal']>$row->sisa_setoran_awal) {
                return [
                    'status'=>'failed',
                    'message'=>'Nilai nominal setoran awal tidak boleh lebih dari sisa setoran awal',
                    'data'=>''];
            }else {
                $sisa_saldo = $row->sisa_setoran_awal - $data['nominal'];

                $this->db->trans_begin();
                $this->db->where("REPLACE(id_tabura,'-','')", str_replace("-", "", $data['id_tabura']));
                $this->db->update('tabura', array('sisa_setoran_awal'=>$sisa_saldo,'status_tabungan'=>'02'));

                $setoran['id_trx'] = getUUID();
                $setoran['id_tabura'] = $data['id_tabura'];
                $setoran['nominal'] = $data['nominal'];
                $setoran['url_bukti'] = $data['url_bukti'];
                $setoran['mekanisme'] = $data['mekanisme'];
                $setoran['catatan'] = $data['catatan'];
                $setoran['tanggal_pembayaran'] = $data['tanggal_pembayaran'];
                $setoran['created_by'] = $data['id_user'];
                $this->db->insert("tabura_setoran_awal", $setoran);

                if ($this->db->trans_status()===false) {
                    $this->db->trans_rollback();
                    return [
                        'status'=>'failed',
                        'message'=>'Setoran awal gagal dilakukan, harap cek ulang data anda',
                        'data'=>''];
                }else {
                    $this->db->trans_commit();
                    return [
                        'status'=>'success',
                        'message'=>'Data setoran awal berhasil ditambahkan',
                        'data'=>'1'];
                }
                $this->db->trans_complete();
            }

        }else {
            return [
                'status'=>'failed',
                'message'=>'Data tabungan umrah tidak ditemukan',
                'data'=>''];
        }
    }

    public function konfirmasiPembayaran($data)
    {
        $konfirmasi['id_konfirmasi'] = getUUID();
        $konfirmasi['jenis_pembayaran'] = $data['jenis_pembayaran'];
        $konfirmasi['nomor_invoice'] = $data['nomor_invoice'];
        $konfirmasi['bank_tujuan'] = $data['bank_tujuan'];
        $konfirmasi['jumlah_dana'] = $data['jumlah_dana'];
        $konfirmasi['tanggal_transfer'] = $data['tanggal_transfer'];
        $konfirmasi['nama_pengirim'] = $data['nama_pengirim'];
        $konfirmasi['email_jamaah'] = $data['email_jamaah'];
        $konfirmasi['no_telpon_jamaah'] = $data['no_telpon_jamaah'];
        $konfirmasi['url_bukti_pembayaran'] = $data['url_bukti'];
        $konfirmasi['status'] = $data['status'];

        $this->db->insert('konfirmasi_pembayaran', $konfirmasi);

        if ($this->db->affected_rows()) {
            return [
                'status'=>'success',
                'message'=>'Konfirmasi pembayaran anda berhasil dan akan diproses dalam 1x24jam. Silakan mengubungi kontak admin untuk melakukan validasi pembayaran anda',
                'data'=>'1'];
        }else {
            return [
                'status'=>'failed',
                'message'=>'Konfirmasi pembayaran anda gagal, harap cek ulang data yang anda masukan untuk memvalidasi data pembayaran',
                'data'=>''];
        }

    }

    public function cekPembayaran($data)
    {
        $data_pembayaran = array();

        if ($data['jenis_pembayaran']=='01' || $data['jenis_pembayaran']=='02' || $data['jenis_pembayaran']=='03') {

            $tabrur = $this->db->query("SELECT sisa_setoran_awal,sisa_target_saldo,nama,nomor_hp, email, status_tabungan  from tabura left join customer on REPLACE(customer.id_customer,'-','')=REPLACE(tabura.id_customer,'-','') where REPLACE(tabura.kode_pembayaran,'-','')=?", array(str_replace("-", "", $data['kode_pembayaran'])));

            if ($tabrur->num_rows()>0) {
                $rs = $tabrur->row();

                $data_pembayaran['type'] = 'tabrur';
                $data_pembayaran['nama'] = $rs->nama;
                $data_pembayaran['nomor_hp'] = $rs->nomor_hp;
                $data_pembayaran['email'] = $rs->email;
                $data_pembayaran['nominal'] = $rs->sisa_setoran_awal;

                return [
                        'status'=>'success',
                        'message'=>'Data ditemukan',
                        'data'=>$data_pembayaran];
            }else {
                return [
                    'status'=>'failed',
                    'message'=>'data pembayaran tabungan umrah tidak ditemukan',
                    'data'=>''];
            }

        } else if($data['jenis_pembayaran']=='04') {

            $umrah = $this->db->query("SELECT nama, nomor_hp, email, total_pembayaran from um_pemesanan
                left join customer on REPLACE(customer.id_customer,'-','')=REPLACE(um_pemesanan.id_customer,'-','')
                where REPLACE(um_pemesanan.kode_pembayaran,'-','')=?", array(str_replace("-", "", $data['kode_pembayaran'])));

            if ($umrah->num_rows()>0) {
                $res = $umrah->row();

                $data_pembayaran['type'] = 'umroh';
                $data_pembayaran['nama'] = $res->nama;
                $data_pembayaran['nomor_hp'] = $res->nomor_hp;
                $data_pembayaran['email'] = $res->email;
                $data_pembayaran['nominal'] = $res->total_pembayaran;

                return [
                    'status'=>'success',
                    'message'=>'Data ditemukan',
                    'data'=>$data_pembayaran];

            }else {
                return [
                    'status'=>'failed',
                    'message'=>'data pembayaran umrah tidak ditemukan',
                    'data'=>''];
            }

        } else if($data['jenis_pembayaran']=='05') {

            $haji = $this->db->query("SELECT nama, nomor_hp, email, uang_muka from hj_pemesanan
             left join customer on REPLACE(customer.id_customer,'-','')=REPLACE(hj_pemesanan.fk_customer,'-','')
             where REPLACE(hj_pemesanan.kode_pemesanan,'-','')=?", array(str_replace("-", "", $data['kode_pembayaran'])));

            if ($haji->num_rows()>0) {
                $res = $haji->row();

                $data_pembayaran['type'] = 'haji';
                $data_pembayaran['nama'] = $res->nama;
                $data_pembayaran['nomor_hp'] = $res->nomor_hp;
                $data_pembayaran['email'] = $res->email;
                $data_pembayaran['nominal'] = $res->uang_muka;

                return [
                    'status'=>'success',
                    'message'=>'Data ditemukan',
                    'data'=>$data_pembayaran];
            }else{
                return [
                    'status'=>'failed',
                    'message'=>'data pembayaran pemesanan paket haji tidak ditemukan',
                    'data'=>''];
            }

        } else if($data['jenis_pembayaran']=='06') {
            $tabah = $this->db->query("SELECT nama_lengkap, nomor_hp, email, sisa_target_tabungan from haji_tabungan
             left join hj_pemesanan_jamaah on REPLACE(hj_pemesanan_jamaah.id_jamaah,'-','')=REPLACE(haji_tabungan.fk_jamaah,'-','')
             where REPLACE(haji_tabungan.kode_reg,'-','')=?", array(str_replace("-", "", $data['kode_pembayaran'])));

            if ($tabah->num_rows()>0) {
                $res = $tabah->row();

                $data_pembayaran['type'] = 'tabah';
                $data_pembayaran['nama'] = $res->nama_lengkap;
                $data_pembayaran['nomor_hp'] = $res->nomor_hp;
                $data_pembayaran['email'] = $res->email;
                $data_pembayaran['nominal'] = $res->sisa_target_tabungan;

                return [
                    'status'=>'success',
                    'message'=>'Data ditemukan',
                    'data'=>$data_pembayaran];
            }else{
                return [
                    'status'=>'failed',
                    'message'=>'data pembayaran pemesanan paket haji tidak ditemukan',
                    'data'=>''];
            }

        } else if($data['jenis_pembayaran']=='07') {
            $tabah = $this->db->query("SELECT nama_lengkap, nomor_hp, email, sisa_setoran_awal from haji_tabungan
             left join hj_pemesanan_jamaah on REPLACE(hj_pemesanan_jamaah.id_jamaah,'-','')=REPLACE(haji_tabungan.fk_jamaah,'-','')
             where REPLACE(haji_tabungan.kode_reg,'-','')=?", array(str_replace("-", "", $data['kode_pembayaran'])));

            if ($tabah->num_rows()>0) {
                $res = $tabah->row();

                $data_pembayaran['type'] = 'tabah';
                $data_pembayaran['nama'] = $res->nama_lengkap;
                $data_pembayaran['nomor_hp'] = $res->nomor_hp;
                $data_pembayaran['email'] = $res->email;
                $data_pembayaran['nominal'] = $res->sisa_setoran_awal;

                return [
                    'status'=>'success',
                    'message'=>'Data ditemukan',
                    'data'=>$data_pembayaran];
            }else{
                return [
                    'status'=>'failed',
                    'message'=>'data pembayaran pemesanan paket haji tidak ditemukan',
                    'data'=>''];
            }

        } else  {

            return [
                    'status'=>'failed',
                    'message'=>'Invoice tidak valid',
                    'data'=>''];

        }

    }






}
