<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require APPPATH.'/libraries/mpdf/autoload.php';

class Api_haji extends CI_Model {

    function __construct() {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getHaji()
    {
        $query = $this->db->query("SELECT id_paket, nama_paket, kategori, durasi_hari, tahun_keberangkatan, uang_muka_usd, uang_muka_idr, foto_paket from haji order by created_at DESC");
        if ($query->num_rows()>0) {

            $i=0;
            foreach ($query->result() as $rows) {
                if ($rows->tahun_keberangkatan>=date('Y')) {
                    $haji[$i]['id_paket'] = str_replace("-", "", $rows->id_paket);
                    $haji[$i]['nama_paket'] = $rows->nama_paket;
                    $haji[$i]['kategori'] = $rows->kategori;
                    $haji[$i]['durasi_hari'] = $rows->durasi_hari;
                    $haji[$i]['tahun_keberangkatan'] = $rows->tahun_keberangkatan;
                    $haji[$i]['uang_muka_usd'] = $rows->uang_muka_usd;
                    $haji[$i]['uang_muka_idr'] = $rows->uang_muka_idr;
                    $haji[$i]['foto_paket'] = base_url($rows->foto_paket);
                    $i++;
                }
            }

            return ['status'=>"success","message"=>"data paket haji tersedia","data"=>$haji];

        } else {
            return ['status'=>'failed','message'=>'Data paket haji belum tersedia','data'=>null];
        }
    }

    public function detail($id)
    {
        $query = $this->db->query("SELECT hj.*, hmk.nama_hotel as hotel_mekah, hmd.nama_hotel as hotel_madinah,
            mkb.nama_maskapai as maskapai_berangkat, mkk.nama_maskapai as maskapai_pulang
         FROM haji hj
         left join m_hotel hmk on REPLACE(hmk.id_hotel,'-','')=REPLACE(hj.id_hotel_mekah,'-','')
         left join m_hotel hmd on REPLACE(hmd.id_hotel,'-','')=REPLACE(hj.id_hotel_madinah,'-','')
         left join m_maskapai mkb on REPLACE(mkb.id_maskapai,'-','')=REPLACE(hj.id_maskapai_berangkat,'-','')
         left join m_maskapai mkk on REPLACE(mkk.id_maskapai,'-','')=REPLACE(hj.id_maskapai_pulang,'-','')
          where REPLACE(hj.id_paket,'-','')=?", array($id));

        if ($query->num_rows()>0) {

            $r = $query->row();
            if ($r->tahun_keberangkatan>=date('Y')) {
                $haji['id_paket'] = str_replace("-", "", $r->id_paket);
                $haji['nama_paket'] = $r->nama_paket;
                $haji['rumah_transit'] = $r->rumah_transit;
                $haji['kategori'] = $r->kategori;
                $haji['durasi_hari'] = $r->durasi_hari;
                $haji['biaya_tidak_termasuk'] = $r->biaya_tidak_termasuk;
                $haji['biaya_termasuk'] = $r->biaya_termasuk;
                $haji['tahun_keberangkatan'] = $r->tahun_keberangkatan;
                $haji['uang_muka_usd'] = $r->uang_muka_usd;
                $haji['uang_muka_idr'] = $r->uang_muka_idr;
                $haji['itinerary'] = $r->itinerary;
                $haji['syarat_ketentuan'] = $r->syarat_ketentuan;
                $haji['ketentuan_pembatalan'] = $r->ketentuan_pembatalan;
                $haji['foto_paket'] = base_url($r->foto_paket);
                $haji['hotel_mekah'] = $r->hotel_mekah;
                $haji['hotel_madinah'] = $r->hotel_madinah;
                $haji['maskapai_berangkat'] = $r->maskapai_berangkat;
                $haji['maskapai_pulang'] = $r->maskapai_pulang;

                return ['status'=>'success','message'=>'data paket haji aktif ditemukan','data'=>$haji];

            } else {
                return ['status'=>'failed','message'=>'tidak ada paket haji aktif','data'=>null];

            }

        } else {
            return ['status'=>'failed','message'=>'data paket haji tidak ditemukan','data'=>null];
        }
    }

    function registrasi($data) {

        $startDate = date('Y-m-d H:i:s');
        $deadline_setoran = date('Y-m-d H:i:s', strtotime('+30 days', strtotime($startDate)));

        $jamaah = array();
        $haji = array();

        $haji['id_pemesanan'] = getUUID();
        $haji['fk_paket_haji'] = $data['paket_haji'];
        $haji['fk_customer'] = $data['customer'];
        $haji['kode_pemesanan'] = auto_code('HAJI-'.date('ym'),"");
        $haji['tanggal_pemesanan'] =date('Y-m-d H:i:s');
        $haji['tanggal_jatuh_tempo'] = $deadline_setoran;
        $haji['fk_cabang'] = '99';
        $haji['bank_tujuan'] = "BNI Syariah";
        $haji['jumlah_jamaah'] = count($data['jamaah']);
        $haji['jenis_pembayaran'] = $data['jenis_pembayaran'];

        if ($data['jenis_pembayaran']=='TUNAI') {
            $haji['total_pembayaran'] = $haji['jumlah_jamaah']*4500;
        } else if($data['jenis_pembayaran']=='TALANGAN'){
            $haji['total_pembayaran'] = $haji['jumlah_jamaah']*2500;
        }
        $haji['uang_muka'] = $haji['total_pembayaran'];
        $haji['created_by'] = $data['id_user'];
        $haji['status_pemesanan'] = '0';
        $haji['jenis_pemesanan'] = $data['jenis_pemesanan'];
        $haji['status_pembayaran'] = 'PENDING';

        $dataKirim['haji'] = $haji;
        $dataKirim['haji']['nama_customer'] = $data['nama_customer'];
        $dataKirim['jamaah'] = $data['jamaah'];

        $mpdf = new \Mpdf\Mpdf(['format' => 'A4']);
        $mpdf->useSubstitutions = false;
        $mpdf->simpleTables = true;
        $mpdf->AddPage('P'); // Adds a new page in Landscape orientationw
        $mpdf->WriteHTML($this->load->view('konfirmasi_pemesanan_haji',$dataKirim, true));
        $mpdf->Output(FCPATH.'files/invoice/'.str_replace("-", "", $haji['kode_pemesanan']).'.pdf','F');

        $haji['url_invoice'] = 'files/invoice/'.str_replace("-", "", $haji['kode_pemesanan']).'.pdf';

        $this->db->set($haji);
        $this->db->trans_begin();
        $this->db->insert('hj_pemesanan', $haji);


        if (count($data['jamaah'])!=0) {
            $j=0;
            for ($i = 0; $i < $haji['jumlah_jamaah']; $i++) {
                $jamaah[$i]['id_jamaah'] = getUUID();
                $jamaah[$i]['fk_pemesanan'] = $haji['id_pemesanan'];
                $jamaah[$i]['nama_lengkap'] = $data['jamaah'][$i]['nama_jamaah'];
                $jamaah[$i]['status_pembayaran'] = 'PENDING';
                $jamaah[$i]['jenis_pembayaran'] = $data['jamaah'][$i]['jenis_pembayaran'];


                $this->db->insert('hj_pemesanan_jamaah', $jamaah[$i]);

                if ($data['jamaah'][$i]['jenis_pembayaran']=='TALANGAN') {
                    $talangan[$j]['id_talangan'] = getUUID();
                    $talangan[$j]['fk_pemesanan_jamaah'] = $jamaah[$i]['id_jamaah'];
                    $talangan[$j]['usd_talangan'] = 2000;
                    $talangan[$j]['idr_talangan'] = $talangan[$j]['usd_talangan']*KONVERSI_USD;
                    $this->db->insert('hj_pemesanan_talangan', $talangan[$j]);
                    $j++;
                }
            }
        }


        if ($this->db->trans_status()===false) {

            $this->db->trans_rollback();
            return [
                'status'=>'failed',
                'message'=>'Pendaftaran paket haji gagal, silahkan cek kembali data anda',
                'data'=>''];
        }else {

            $this->db->trans_commit();
            $mail = new PHPMailer();

            $dataKirim['haji'] = $haji;
            $dataKirim['haji']['nama_customer'] = $data['nama_customer'];
            $dataKirim['jamaah'] = $jamaah;

            $html = $this->load->view('kirim_pemesanan_haji',$dataKirim, true);
            $mail->IsHTML(true);    // set email format to HTML
            $mail->IsSMTP();   // we are going to use SMTP
            $mail->SMTPAuth   = true; // enabled SMTP authentication
            $mail->SMTPSecure = "ssl"; // prefix for secure protocol to connect to the server
            $mail->SMTPAutoTLS = false;
            // $mail->SMTPDebug  = 2;
            $mail->Host       = "smtp.gmail.com"; // setting GMail as our SMTP server
            $mail->Port       = 465;   // SMTP port to connect to GMail
            $mail->Username   = "cyber.rosana@gmail.com";  // alamat email kamu
            $mail->Password   = "www.rosanatourtravel.com1";            // password GMail
            $mail->SetFrom("cyber.rosana@gmail.com", 'Penting !');  //Siapa yg mengirim email
            $mail->Subject    = 'Invoice pesanan anda';
            $mail->Body       = $html;
            $mail->AddAddress($data['email']);
            $mail->WordWrap = 50;
            $mail->Priority = 1;
            $mail->AddCustomHeader("X-MSMail-Priority: High");

            if(!$mail->Send()) {
                echo $mail->ErrorInfo;
            }

            $mail->ClearAddresses();

            return [
                'status'=>'success',
                'message'=>'Pendaftaran paket haji berhasil dilakukan',
                'data'=>'1'];
        }
        $this->db->trans_complete();
    }

    public function getPesanan($data)
    {
        $this->db->select("hp.*, hj.nama_paket, hj.kategori");
        $this->db->from('hj_pemesanan hp');
        $this->db->join("haji hj","REPLACE(hj.id_paket,'-','')=REPLACE(hp.fk_paket_haji,'-','')","left");
        $this->db->where("REPLACE(hp.fk_customer,'-','')", str_replace("-", "", $data['id_customer']));
        $this->db->where("hp.jenis_pembayaran", $data['jenis_pembayaran']);
        $result = $this->db->get();

        if ($result->num_rows()>0) {

            $paket=array();
            $jamaah=array();
            $no=0;

            foreach ($result->result() as $rows) {
                $paket[$no]['id_pemesanan'] = str_replace("-", "", $rows->id_pemesanan);
                $paket[$no]['nama_paket'] = $rows->nama_paket;
                $paket[$no]['kode_pemesanan'] = $rows->kode_pemesanan;
                $paket[$no]['tanggal_pemesanan'] = $rows->tanggal_pemesanan;
                $paket[$no]['tanggal_jatuh_tempo'] = $rows->tanggal_jatuh_tempo;
                $paket[$no]['jumlah_jamaah'] = $rows->jumlah_jamaah;
                $paket[$no]['total_pembayaran'] = $rows->total_pembayaran;
                $paket[$no]['jenis_pemesanan'] = $rows->jenis_pemesanan;
                $paket[$no]['status_pembayaran'] = $rows->status_pembayaran;

                $get = $this->db->query("SELECT id_jamaah,fk_pemesanan, nama_lengkap, jenis_identitas, nomor_identitas, jenis_pembayaran from hj_pemesanan_jamaah where REPLACE(fk_pemesanan, '-','')=?", array($paket[$no]['id_pemesanan']));

                $i=0;
                foreach ($get->result() as $row) {
                    $jamaah[$i]['id_jamaah'] = str_replace("-", "", $row->id_jamaah);
                    $jamaah[$i]['fk_pemesanan'] = $row->fk_pemesanan;
                    $jamaah[$i]['nama_lengkap'] = $row->nama_lengkap;
                    $jamaah[$i]['identitas'] = $row->jenis_identitas.' - '.$row->nomor_identitas;
                    $jamaah[$i]['jenis_pembayaran'] = $row->jenis_pembayaran;
                    $i++;
                }

                $paket[$no]['jamaah'] = $jamaah;

                $no++;
            }

            return ['status'=>'success','message'=>'data pesanan paket haji ditemukan','data'=>$paket];

        }else {
            return ['status'=>'failed','message'=>'data pesanan paket anda masih kosong','data'=>null];
        }
    }

    public function detailPesanan($id)
    {
        $this->db->select("hp.*, hj.nama_paket, hj.kategori");
        $this->db->from('hj_pemesanan hp');
        $this->db->join("haji hj","REPLACE(hj.id_paket,'-','')=REPLACE(hp.fk_paket_haji,'-','')","left");
        $this->db->where("REPLACE(hp.id_pemesanan,'-','')", str_replace("-", "", $id));
        $result = $this->db->get();

        if ($result->num_rows()>0) {

            $paket=array();
            $jamaah=array();
            $pembayaran=array();
            $i=0;
            $j=0;

            $rows = $result->row();

            $paket['id_pemesanan'] = str_replace("-", "", $rows->id_pemesanan);
            $paket['nama_paket'] = $rows->nama_paket;
            $paket['kode_pemesanan'] = $rows->kode_pemesanan;
            $paket['tanggal_pemesanan'] = $rows->tanggal_pemesanan;
            $paket['tanggal_jatuh_tempo'] = $rows->tanggal_jatuh_tempo;
            $paket['jumlah_jamaah'] = $rows->jumlah_jamaah;
            $paket['total_pembayaran'] = $rows->total_pembayaran;
            $paket['jenis_pemesanan'] = $rows->jenis_pemesanan;
            $paket['status_pembayaran'] = $rows->status_pembayaran;

            $get = $this->db->query("SELECT * from hj_pemesanan_jamaah where REPLACE(fk_pemesanan, '-','')=?",
             array(str_replace("-", "", $rows->id_pemesanan)));

            if ($get->num_rows()>0) {
                foreach ($get->result() as $row) {

                    $tabungan = $this->db->query("SELECT * FROM haji_tabungan where REPLACE(fk_jamaah,'-','')=?", array(str_replace("-", "", $row->id_jamaah)));

                    if ($tabungan->num_rows()>0) {
                        $jamaah[$i]['is_jamaah_tabungan'] = '1';
                    } else {
                        $jamaah[$i]['is_jamaah_tabungan'] = '0';
                    }
                    $jamaah[$i]['id_jamaah'] = str_replace("-", "", $row->id_jamaah);
                    $jamaah[$i]['nama_lengkap'] = $row->nama_lengkap;
                    $jamaah[$i]['identitas'] = $row->jenis_identitas.' - '.$row->nomor_identitas;
                    $jamaah[$i]['jenis_pembayaran'] = $row->jenis_pembayaran;
                    $jamaah[$i]['jenis_identitas'] = $row->jenis_identitas;
                    $jamaah[$i]['nomor_identitas'] = $row->nomor_identitas;
                    $jamaah[$i]['jenis_kelamin'] = $row->jenis_kelamin;
                    $jamaah[$i]['tempat_lahir'] = $row->tempat_lahir;
                    $jamaah[$i]['tanggal_lahir'] = $row->tanggal_lahir;
                    $jamaah[$i]['status_pembayaran'] = $row->status_pembayaran;
                    $i++;
                }

            }

            $histori = $this->db->query("SELECT * from hj_pemesanan_setoran where REPLACE(fk_pemesanan, '-','')=?",
             array(str_replace("-", "", $rows->id_pemesanan)));

            if ($histori->num_rows()>0) {
                foreach ($histori->result() as $row) {

                    $pembayaran[$j]['id_trx'] = str_replace("-", "", $row->id_trx);
                    $pembayaran[$j]['tanggal_pembayaran'] = $row->tanggal_pembayaran;
                    $pembayaran[$j]['mekanisme'] = $row->mekanisme;
                    $pembayaran[$j]['nominal_usd'] = $row->nominal_usd;
                    $pembayaran[$j]['nominal'] = $row->nominal;
                    $pembayaran[$j]['catatan'] = $row->catatan;
                    $pembayaran[$j]['is_valid'] = $row->is_valid;
                    $j++;
                }

            }

            $paket['jamaah'] = $jamaah;
            $paket['pembayaran'] = $pembayaran;

            return ['status'=>'success','message'=>'data pesanan paket haji ditemukan','data'=>$paket];

        }else {
            return ['status'=>'failed','message'=>'data pesanan paket anda masih kosong','data'=>null];
        }
    }

    public function editJamaah($id_jamaah)
    {
        $query = $this->db->query("SELECT * FROM hj_pemesanan_jamaah where REPLACE(id_jamaah,'-','')=?", array($id_jamaah));
        if ($query->num_rows()!=0) {

            $row = $query->row();

            $tabungan = $this->db->query("SELECT * FROM haji_tabungan where REPLACE(fk_jamaah,'-','')=?", array($id_jamaah));

            if ($tabungan->num_rows()>0) {
                $jamaah['is_jamaah_tabungan'] = '1';
            } else {
                $jamaah['is_jamaah_tabungan'] = '0';
            }
            $jamaah['id_jamaah'] = str_replace("-", "", $row->id_jamaah);
            $jamaah['fk_pemesanan'] = str_replace("-", "", $row->fk_pemesanan);
            $jamaah['nama_lengkap'] = $row->nama_lengkap;
            $jamaah['jenis_identitas'] = $row->jenis_identitas;
            $jamaah['nomor_identitas'] = $row->nomor_identitas;
            $jamaah['jenis_kelamin'] = $row->jenis_kelamin;
            $jamaah['tempat_lahir'] = $row->tempat_lahir;
            $jamaah['tanggal_lahir'] = $row->tanggal_lahir;
            $jamaah['status_kawin'] = $row->status_kawin;
            $jamaah['pendidikan'] = $row->pendidikan;
            $jamaah['pekerjaan'] = $row->pekerjaan;
            $jamaah['id_provinsi'] = $row->id_provinsi;
            $jamaah['id_kabupaten'] = $row->id_kabupaten;
            $jamaah['id_kecamatan'] = $row->id_kecamatan;
            $jamaah['alamat_ktp'] = $row->alamat_ktp;
            $jamaah['alamat_domisili'] = $row->alamat_domisili;
            $jamaah['golongan_darah'] = $row->golongan_darah;
            $jamaah['nama_ayah'] = $row->nama_ayah;
            $jamaah['nama_ibu'] = $row->nama_ibu;
            $jamaah['email'] = $row->email;
            $jamaah['nomor_hp'] = $row->nomor_hp;
            $jamaah['nomor_hp_cadangan'] = $row->nomor_hp_cadangan;
            $jamaah['nama_ahli_waris'] = $row->nama_ahli_waris;
            $jamaah['nomor_hp_ahli_waris'] = $row->nomor_hp_ahli_waris;
            $jamaah['alamat_ahli_waris'] = $row->alamat_ahli_waris;
            $jamaah['kewarganegaraan'] = $row->kewarganegaraan;
            $jamaah['id_referensi_pegawai'] = $row->id_referensi_pegawai;

            if ($row->foto_profil!='' || $row->foto_profil!=NULL) {
                $jamaah['foto_profil'] = base_url($row->foto_profil);
            } else {
                $jamaah['foto_profil'] = NULL;
            }

            return [
                'status'=>'success',
                'message'=>'Data jamaah berhasil ditemukan',
                'data'=>$jamaah];

        }else {
            return [
                'status'=>'failed',
                'message'=>'data jamaah tidak ditemukan',
                'data'=>null];
        }
    }

    public function updateJamaah($data, $id)
    {
        $this->db->where('REPLACE(id_jamaah,"-","")', $id);
        $this->db->update("hj_pemesanan_jamaah", $data);
        if ($this->db->affected_rows()) {
            return [
                'status'=>'success',
                'message'=>'Data jamaah pemesanan paket haji berhasil diupdate',
                'data'=>'1'];
        }else {
            return [
                'status'=>'failed',
                'message'=>'Data jamaah pemesanan paket haji gagal diupdate',
                'data'=>'0'];
        }
    }

    public function detailTabah($id_jamaah)
    {
        $query = $this->db->query("SELECT ht.*, hj.fk_pemesanan,hj.id_jamaah, hj.jenis_identitas, hj.nomor_identitas, hj.nama_lengkap, hj.nomor_hp, hj.email from haji_tabungan ht
            left join hj_pemesanan_jamaah hj on REPLACE(hj.id_jamaah,'-','')=REPLACE(ht.fk_jamaah,'-','') where REPLACE(ht.fk_jamaah,'-','')=?", array(str_replace("-", "", $id_jamaah)));

        if ($query->num_rows()>0) {

            $row = $query->row();

            $tabah['id_tabungan'] = str_replace("-", "", $row->id_tabungan);
            $tabah['fk_pemesanan'] = str_replace("-", "", $row->fk_pemesanan);
            $tabah['kode_reg'] = $row->kode_reg;
            $tabah['nomer_va'] = $row->nomer_va;
            $tabah['target_tabungan'] = $row->target_tabungan;
            $tabah['sisa_target_tabungan'] = $row->sisa_target_tabungan;
            $tabah['setoran_awal'] = $row->setoran_awal;
            $tabah['sisa_setoran_awal'] = $row->sisa_setoran_awal;
            $tabah['setoran_perbulan'] = $row->setoran_perbulan;
            $tabah['status_tabungan'] = $row->status_tabungan;
            $tabah['jenis_identitas'] = $row->jenis_identitas;
            $tabah['nomor_identitas'] = $row->nomor_identitas;
            $tabah['nama_lengkap'] = $row->nama_lengkap;
            $tabah['nomor_hp'] = $row->nomor_hp;
            $tabah['email'] = $row->email;

            $histori=array();

            $tabungan = $this->db->query("SELECT * FROM haji_tabungan_setoran where REPLACE(id_tabungan_haji,'-','')=?", array($tabah['id_tabungan']));
            $i=0;
            if ($tabungan->num_rows()>0) {
              foreach ($tabungan->result() as $rows) {
                    $histori[$i]['id_trx'] = str_replace("-", "", $rows->id_trx);
                    $histori[$i]['tanggal_pembayaran'] = $rows->tanggal_pembayaran;
                    $histori[$i]['mekanisme'] = $rows->mekanisme;
                    $histori[$i]['jenis_setoran'] = $rows->jenis_setoran;
                    $histori[$i]['nominal'] = $rows->nominal;
                    $histori[$i]['catatan'] = $rows->catatan;
                    $histori[$i]['url_bukti'] = base_url($rows->url_bukti_pembayaran);
                    $i++;
                }
            }

            $tabah['histori'] = $histori;

            return [
                'status'=>'success',
                'message'=>'Data tabungan pelunasan haji '.ucwords(strtolower($tabah['nama_lengkap'])).' ditemukan.',
                'data'=>$tabah];

        } else {
            return [
                'status'=>'failed',
                'message'=>'Data tabungan pelunasan haji tidak ditemukan',
                'data'=>NULL];
        }
    }

    public function listPendaftarTabah($id_customer)
    {
        if ($id_customer!='' || $id_customer!=NULL) {
            $get = $this->db->query("
                SELECT ht.id_tabungan, ht.kode_reg, ht.nomer_va,
                hjmh.id_jamaah,hjmh.nama_lengkap, hjmh.jenis_identitas, hjmh.nomor_identitas,hjmh.jenis_kelamin
                from haji_tabungan ht
                left join hj_pemesanan_jamaah hjmh on REPLACE(hjmh.id_jamaah,'-','')=REPLACE(ht.fk_jamaah,'-','')
                left join hj_pemesanan hp on REPLACE(hp.id_pemesanan,'-','') = REPLACE(hjmh.fk_pemesanan,'-','')
                where REPLACE(hp.fk_customer,'-','')=?", array(str_replace("-", "", $id_customer)));

            if ($get->num_rows()>0) {
                $i=0;
                foreach ($get->result() as $rows) {
                    $tabah[$i]['id_tabungan'] = str_replace("-", "", $rows->id_tabungan);
                    $tabah[$i]['id_jamaah'] = str_replace("-", "", $rows->id_jamaah);
                    $tabah[$i]['kode_reg'] = $rows->kode_reg;
                    $tabah[$i]['nomer_va'] = $rows->nomer_va;
                    $tabah[$i]['nama_lengkap'] = $rows->nama_lengkap;
                    $tabah[$i]['jenis_identitas'] = $rows->jenis_identitas;
                    $tabah[$i]['nomor_identitas'] = $rows->nomor_identitas;
                    $tabah[$i]['jenis_kelamin'] = $rows->jenis_kelamin;
                    $i++;
                }

                return [
                    'status'=>'success',
                    'message'=>'Data pendaftaran tabungan pelunasan haji ditemukan',
                    'data'=>$tabah];

            } else {
                return [
                    'status'=>'failed',
                    'message'=>'Data tidak ditemukan',
                    'data'=>''];
            }
        } else {
            return [
                'status'=>'failed',
                'message'=>'Data parameter tidak boleh kosong',
                'data'=>''];
        }
    }

    public function cekTabunganJamaah($id_jamaah)
    {
        $this->db->select("*");
        $this->db->from("haji_tabungan");
        $this->db->where("REPLACE(fk_jamaah,'-','')", str_replace("-", "", $id_jamaah));
        return $this->db->count_all_results();
    }


}
