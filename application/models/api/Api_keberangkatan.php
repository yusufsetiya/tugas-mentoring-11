<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_keberangkatan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function keberangkatan_data() {
        $get = $this->db->query("
            SELECT id_grup, nama_grup, is_plus_tour, tanggal_keberangkatan,
            durasi_hari,batas_pendaftaran from um_grup_keberangkatan order by created_at DESC
        ");
        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Data tidak ditemukan."];
        }

        $i = 0;
        foreach ($get->result() as $key => $r) {
            $result[$i]['id_grup'] = $r->id_grup;
            $result[$i]['nama_grup'] = $r->nama_grup;
            $result[$i]['is_plus_tour'] = $r->is_plus_tour;
            $result[$i]['tanggal_keberangkatan'] = $r->tanggal_keberangkatan;
            $result[$i]['durasi_hari'] = $r->durasi_hari;
            $result[$i]['batas_pendaftaran'] = $r->batas_pendaftaran;
            $i++;
        }

        // serve
        return ["status" => "ok", "data" => $result];
    }

    function keberangkatan_search($keyword)
    {
        $sql = $this->db->query( "SELECT id_grup, nama_grup, is_plus_tour, tanggal_keberangkatan,
            durasi_hari,batas_pendaftaran FROM um_grup_keberangkatan where nama_grup like '%".$keyword."%'");
        if ($sql->num_rows()==0) {
            return ['status'=>'failed','message'=>'data tidak ditemukan.','data'=>'0'];

        }else {

            $i=0;
            foreach ($sql->result() as $key => $r) {
                $result[$i]['id_grup'] = $r->id_grup;
                $result[$i]['nama_grup'] = $r->nama_grup;
                $result[$i]['is_plus_tour'] = $r->is_plus_tour;
                $result[$i]['tanggal_keberangkatan'] = $r->tanggal_keberangkatan;
                $result[$i]['durasi_hari'] = $r->durasi_hari;
                $result[$i]['batas_pendaftaran'] = $r->batas_pendaftaran;
                $i++;
            }

            return ['status'=>'ok','message'=>'data ditemukan.','data'=>$result];
        }
    }

    public function get_detail($id_keberangkatan)
    {
        $grup = $this->db->query("SELECT * from um_grup_keberangkatan where REPLACE(id_grup,'-','')=REPLACE('$id_keberangkatan','-','')");
        if ($grup->num_rows()!=0) {

            $k = $grup->row();

            $result['id_grup'] = $k->id_grup;
            $result['fk_kategori_paket'] = $k->fk_kategori_paket;
            $result['nama_grup'] = $k->nama_grup;
            $result['is_plus_tour'] = $k->is_plus_tour;
            $result['tanggal_keberangkatan'] = $k->tanggal_keberangkatan;
            $result['durasi_hari'] = $k->durasi_hari;
            $result['tanggal_pulang'] = $k->tanggal_pulang;
            $result['batas_pendaftaran'] = $k->batas_pendaftaran;
            $result['syarat_ketentuan'] = $k->syarat_ketentuan;
            $result['ketentuan_pembatalan'] = $k->ketentuan_pembatalan;

            $dtmaskapai = $this->db->query("SELECT nama_maskapai, fasilitas from um_grup_keberangkatan_maskapai left join m_maskapai on m_maskapai.id_maskapai=um_grup_keberangkatan_maskapai.fk_maskapai where um_grup_keberangkatan_maskapai.fkid_grup_keberangkatan='$id_keberangkatan'");
            $maskapai = array();
            $j=0;
            foreach ($dtmaskapai->result() as $key => $r) {
                $maskapai[$j]['nama_maskapai'] = $r->nama_maskapai;
                $maskapai[$j]['fasilitas'] = $r->fasilitas;
                $j++;
            }


            $dtrute = $this->db->query("SELECT iata, nama_bandara, kota, type from um_grup_keberangkatan_rute left join m_bandara on m_bandara.id_bandara=um_grup_keberangkatan_rute.fk_id_bandara where um_grup_keberangkatan_rute.fk_grup_keberangkatan='$id_keberangkatan' order by type ASC");
            $rute = array();
            $i=0;
            foreach ($dtrute->result() as $key => $r) {
                $rute[$i]['iata'] = $r->iata;
                $rute[$i]['nama_bandara'] = $r->nama_bandara;
                $rute[$i]['kota'] = $r->kota;
                $rute[$i]['type'] = $r->type;
                $i++;
            }

            $result['maskapai'] = $maskapai;
            $result['rute'] = $rute;

            return ['status'=>'ok','message'=>'data ditemukan.','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'data tidak ditemukan.','data'=>'0'];
        }
    }

}
