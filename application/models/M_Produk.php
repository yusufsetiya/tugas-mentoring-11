<?php defined('BASEPATH') or exit('no access allowed');
/**
 * summary
 */
class M_Produk extends MY_Model
{
    /**
     * summary
     */
    protected $_table_name = "product";
    protected $_order_by = "id";
    protected $_order_by_type = "ASC";
    protected $_primary_key = "id";


    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getData()
    {
        $this->db->select('product.*, categories.id, categories.nama as katNama');
        $this->db->from('product');
        $this->db->join('categories', 'categories.id = product.categories_id');
        $this->db->order_by('product.id', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function getKategori()
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->order_by('categories.id', 'ASC');
        $query = $this->db->get();
        return $query;
    }

    // public function getDetail()
    // {
    //     $this->db->select('product.*, categories.id, categories.name as katNama');
    //     $this->db->from('product');
    //     $this->db->join('categories', 'categories.id = product.category_id');
    //     $this->db->order_by('product.id', 'ASC');
    //     $query = $this->db->get();
    //     return $query;
    // }

    //buatkan function untuk mengambil data berdasarkan id
    public function getDetail($id)
    {
        $this->db->select('product.*, categories.id, categories.name as katNama');
        $this->db->from('product');
        $this->db->join('categories', 'categories.id = product.category_id');
        $this->db->where('product.id', $id);
        $query = $this->db->get()->row_array();
        return $query;
    }
}
