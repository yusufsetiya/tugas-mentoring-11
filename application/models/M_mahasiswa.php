<?php defined('BASEPATH') or exit('no access allowed');
/**
 * summary
 */
class M_mahasiswa extends MY_Model
{
    /**
     * summary
     */
    protected $_table_name = "mahasiswa";
    protected $_order_by = "id";
    protected $_order_by_type = "ASC";
    protected $_primary_key = "id";


    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getData()
    {
        $this->db->select('mahasiswa.id as idMhs, mahasiswa.nama as namaMhs, mahasiswa.nim as nimMhs, mahasiswa.jenis_kelamin as jenkel, mahasiswa.alamat as alamatMhs, mahasiswa_hobi.*, GROUP_CONCAT(ref_hobi.hobi) AS namaHobi');
        $this->db->from('mahasiswa');
        $this->db->join('mahasiswa_hobi', 'mahasiswa_hobi.id_mahasiswa = mahasiswa.id');
        $this->db->join('ref_hobi', 'ref_hobi.id = mahasiswa_hobi.id_hobi');
        $this->db->group_by('mahasiswa.id');
        // $this->db->order_by('product.id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getHobi()
    {
        $this->db->select('*');
        $this->db->from('ref_hobi');
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function simpanMahasiswa($data)
    {
        $this->db->trans_start();

        try {
            $this->db->insert('mahasiswa', [
                'nim' => $data['nim'],
                'nama' => $data['nama'],
                'jenis_kelamin' => $data['jenkel'],
                'alamat' => $data['alamat']
            ]);

            $id = $this->db->insert_id();
            $hobi = [];
            foreach ($data['hobi'] as $key => $value) {
                $hobi[] = [
                    'id_mahasiswa' => $id,
                    'id_hobi' => $value
                ];
            }

            $this->db->insert_batch('mahasiswa_hobi', $hobi);

            $this->db->trans_commit();
            $res = [
                'status' => true,
                'message' => 'Data mahasiswa berhasil disimpan.'
            ];
            return $res;
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $res = [
                'status' => false,
                'message' => log_message('error', $e->getMessage()) . show_error('An error occurred while saving data.')
            ];
            return $res;
        }
    }

    public function getDetail($id)
    {
        $this->db->select('mahasiswa.id as idMhs, mahasiswa.alamat as alamatMhs, mahasiswa.nama as namaMhs, mahasiswa.nim as nimMhs, mahasiswa.jenis_kelamin as jenkel, mahasiswa.alamat as alamatMhs, mahasiswa_hobi.id as idHobi, GROUP_CONCAT(ref_hobi.id) AS hobi');
        $this->db->from('mahasiswa');
        $this->db->join('mahasiswa_hobi', 'mahasiswa_hobi.id_mahasiswa = mahasiswa.id');
        $this->db->join('ref_hobi', 'ref_hobi.id = mahasiswa_hobi.id_hobi');
        $this->db->group_by('mahasiswa.id');
        $this->db->where('mahasiswa.id', $id);
        // $this->db->order_by('product.id', 'ASC');
        $query = $this->db->get()->row_array();
        return $query;
    }

    public function updateMahasiswa($id, $data)
    {
        $this->db->trans_start();

        try {
            $this->db->where('id', $id);
            $this->db->update('mahasiswa', [
                'nim' => $data['nim'],
                'nama' => $data['nama'],
                'jenis_kelamin' => $data['jenkel'],
                'alamat' => $data['alamat']
            ]);

            $id = $id;
            $this->db->delete('mahasiswa_hobi', ['id_mahasiswa' => $id]);

            $hobi = [];
            foreach ($data['hobi'] as $key => $value) {
                $hobi[] = [
                    'id_mahasiswa' => $id,
                    'id_hobi' => $value
                ];
            }

            $this->db->insert_batch('mahasiswa_hobi', $hobi);

            $this->db->trans_commit();
            $res = [
                'status' => true,
                'message' => 'Data mahasiswa berhasil diperbarui.'
            ];
            return $res;
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $res = [
                'status' => false,
                'message' => log_message('error', $e->getMessage()) . show_error('An error occurred while saving data.')
            ];
            return $res;
        }
    }

    public function deleteMahasiswa($id)
    {
        $this->db->trans_start();

        try {
            $this->db->where('id_mahasiswa', $id);
            $this->db->delete('mahasiswa_hobi');

            $this->db->where('id', $id);
            $this->db->delete('mahasiswa');


            $this->db->trans_commit();
            $res = [
                'status' => true,
                'message' => 'Data mahasiswa berhasil Dihapus.'
            ];
            return $res;
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $res = [
                'status' => false,
                'message' => log_message('error', $e->getMessage()) . show_error('An error occurred while Delete data.')
            ];
            return $res;
        }
    }
}
