<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "mahasiswa";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_mahasiswa');
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Mahasiswa';
        $this->data['condition'] = '<div class="breadcrumb-item active">Mahasiswa</div>';

        $this->data['mahasiswa'] = $this->M_mahasiswa->getData();
        $this->data['hobiAll'] = $this->M_mahasiswa->getHobi();

        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('mahasiswa/index/index', $this->data);
        $this->layout->publish();
    }

    public function do_save()
    {
        $data['nama'] = $this->input->post('nama');
        $data['nim'] = $this->input->post('nim');
        $data['jenkel'] = $this->input->post('jenkel');
        $data['alamat'] = $this->input->post('alamat');
        $data['hobi'] = $this->input->post('hobi');

        $simpan = $this->M_mahasiswa->simpanMahasiswa($data);

        if ($simpan['status'] == 'success') {
            $this->session->set_flashdata('success', $simpan['message']);
            redirect('mahasiswa/index');
        } else {
            $this->session->set_flashdata('error', $simpan['message']);
            redirect('mahasiswa/index');
        }
    }

    public function viewUpdate()
    {
        $id = $this->uri->segment(4);
        $this->data['nama_menu'] = 'Edit Mahasiswa';
        $this->data['condition'] = '<div class="breadcrumb-item active">Mahasiswa</div>';

        $this->data['mahasiswa'] = $this->M_mahasiswa->getDetail($id);
        $this->data['hobiAll'] = $this->M_mahasiswa->getHobi();

        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('mahasiswa/index/detail', $this->data);
        $this->layout->publish();
    }

    public function do_update($id)
    {
        $data['nama'] = $this->input->post('nama');
        $data['nim'] = $this->input->post('nim');
        $data['jenkel'] = $this->input->post('jenkel');
        $data['alamat'] = $this->input->post('alamat');
        $data['hobi'] = $this->input->post('hobi');

        $update = $this->M_mahasiswa->updateMahasiswa($id, $data);

        if ($update['status'] == 'success') {
            $this->session->set_flashdata('success', $update['message']);
            redirect('mahasiswa/index');
        } else {
            $this->session->set_flashdata('error', $update['message']);
            redirect('mahasiswa/index');
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(4);
        $delete = $this->M_mahasiswa->deleteMahasiswa($id);

        if ($delete['status'] == 'success') {
            $this->session->set_flashdata('success', $delete['message']);
            redirect('mahasiswa/index');
        } else {
            $this->session->set_flashdata('error', $delete['message']);
            redirect('mahasiswa/index');
        }
    }
}
