<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "categories";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Kategori';
        $this->data['condition'] = '<div class="breadcrumb-item active">Produk</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('categories');

        $crud->columns(['name']);
        $crud->addFields(['name']);
        $crud->editFields(['name']);
        $crud->requiredFields(['name']);
        $crud->defaultOrdering('id', 'desc');
        $display = [
            'name' => 'Nama Kategori',
        ];

        $crud->displayAs($display);
        $crud->callbackBeforeInsert(function ($stateParameters) {
            $stateParameters->data['id'] = generateUUID();
            return $stateParameters;
        });

        $crud->unsetJquery();
        $output = $crud->render();

        $this->_setOutput($output, 'index');
    }

    // public function index()
    // {
    //     $this->data['nama_menu'] = 'Data Kategori';
    //     $this->data['condition'] = '<div class="breadcrumb-item active">Kategori</div>';

    //     //load model tampil data
    //     $this->load->model('M_Produk');
    //     $this->data['produk'] = $this->M_Produk->getKategori()->result();

    //     $this->layout->set_template('template/app');
    //     $this->layout->CONTENT->view('kategori/index/index', $this->data);
    //     $this->layout->publish();
    // }
}
