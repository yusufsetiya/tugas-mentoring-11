<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController {

    protected $template = "app";

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['halaman'] = "beranda";
        $this->data['nama_menu']='Dashboard '.$this->config->item('webname');
        $this->render("index");
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function send_email_aktifasi($recipient, $subyek, $message) {
        date_default_timezone_set('Asia/Jakarta');

        $data = array('recipient' => $recipient, 'subyek' => $subyek, 'message' => $message);
        $options = array(
            'http' => array(
                'header' => "Content-type: application/json\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($this->email_handler->url(), false, $context);

        if (strpos($result, "email sent!") == true) {
            return true;
        } else {
            return false;
        }

    }

    public function logout() {
        $id_user = $this->session->userdata('admin_userId');
        $activity = "LOG OUT";
        $page_url = base_url("?logout=true");
        $this->session->unset_userdata($this->session->all_userdata());

        $this->m_activity_log->insert($id_user, $activity, $page_url);

        $this->unSetUserData();
        $this->load->database("default", FALSE, TRUE); //CHANGE DB TO DEFAULT sialogin
        redirect(site_url('login')."?logout=true");
    }

    public function set_admin() {
        $this->session->set_userdata('triggered', '1');
        $this->session->set_userdata("t_groupName", "JASKO");
        redirect("index");
    }

    public function cekAPI()
    {
        $endpoin = 'http://yess.bppsdmp.pertanian.go.id/mis2/_api/users/nik';

        $request  = curl_init($endpoin);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, ['nik'=>'3213231207800006']);

        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($request);
        curl_close($request);

        $result = json_decode($result);
        echo '<pre>';
        print_r($result);
        exit();
    }

    function ws_post($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}
