<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "devisi";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_devisi",'devisi');
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Referensi Devisi';
        $this->data['condition'] = '<div class="breadcrumb-item active">Devisi</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('ref_devisi');


        $crud->columns(['kode_devisi','nama_devisi']);

        $crud->fields(['kode_devisi','nama_devisi']);
        // $crud->editFields(['nama_devisi']);
        $crud->requiredFields(['kode_devisi','nama_devisi']);
        $crud->defaultOrdering('nama_devisi', 'asc');

        $crud->displayAs('kode_devisi', 'Kode Devisi');
        $crud->displayAs('nama_devisi', 'Devisi');
        $crud->callbackBeforeInsert([$this,'_callBeforeInsert']);

        $crud->unsetJquery();
        $output = $crud->render();

        $this->_setOutput($output,'index');
    }

    public function _callBeforeInsert($params)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $check = $this->devisi->cekKode($params->data['kode_devisi']);
        if (!$check['success']) {
            return $errorMessage->setMessage($check['message']);
        } else {
            $params->data['id'] = getUUID();
        }
        
        return $params;
    }

    function _setOutput($output = null, $view=null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);
        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('devisi/index/'.$view, $x);
        $this->layout->publish();
    }

}
