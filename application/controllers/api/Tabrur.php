<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Tabrur extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/api_tabrur', 'tabrur');
    }

    public function register_post()
    {
        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= FALSE && property_exists($decoded_token, "id_user")) {


                if ($this->input->post('fk_id_user')!='' || $this->input->post('fk_id_user')!= NULL) {

                $tanggal_sekarang = date('Y-m-d');
                $deadline_setoran = date('Y-m-d', strtotime('+30 days', strtotime($tanggal_sekarang)));

                $url_ktp = '';
                $data_lama = $this->tabrur->getCustomer($this->input->post('fk_id_user'));

                if ($data_lama->url_ktp !='' || $data_lama->url_ktp!= NULL) {

                    if (file_exists('./'.$data_lama->url_ktp)) {
                        unlink('./'.$data_lama->url_ktp);
                    }
                }

                if (isset($_FILES['url_ktp']['name'])) {
                    list($width, $height) = getimagesize($_FILES['url_ktp']['tmp_name']);
                    $config['upload_path'] = 'files/profil/'; //path folder file upload
                    $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                    $config['max_size'] = '2000';
                    $config['file_name'] = "ktp_" . date('ymdhis'); //enkripsi file name upload
                    $this->load->library('upload');
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('url_ktp')) {
                        $file_foto = $this->upload->data();
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = './files/profil/' . $file_foto['file_name'];
                        $config['create_thumb'] = FALSE;
                        $config['maintain_ratio'] = TRUE;
                        $config['quality'] = '50%';
                        $config['width'] = round($width / 2);
                        $config['height'] = round($height / 2);
                        $config['new_image'] = './files/profil/' . $file_foto['file_name'];
                        $this->load->library('image_lib');
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                        $nama_foto = 'files/profil/' . $file_foto['file_name'];
                        $url_ktp = $nama_foto;
                    }
                }

                $data['nik'] = $this->input->post('nik');
                $data['nama'] = $this->input->post('nama');
                $data['nama_ayah'] = $this->input->post('nama_ayah');
                $data['tempat_lahir'] = $this->input->post('tempat_lahir');
                $data['tanggal_lahir'] = $this->input->post('tanggal_lahir');
                $data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
                $data['status'] = $this->input->post('status');
                $data['nomor_hp'] = $this->input->post('nomor_hp');
                $data['email'] = $this->input->post('email');
                $data['pendidikan'] = $this->input->post('pendidikan');
                $data['pekerjaan'] = $this->input->post('pekerjaan');
                $data['penghasilan'] = $this->input->post('penghasilan');
                $data['id_provinsi'] = $this->input->post('id_provinsi');
                $data['id_kabupaten'] = $this->input->post('id_kabupaten');
                $data['alamat'] = $this->input->post('alamat');
                $data['fk_id_user'] = $this->input->post('fk_id_user');
                $data['id_customer'] = $this->input->post('id_customer');
                $data['syarat_ketentuan'] = $this->input->post('syarat_ketentuan');
                $data['status_tabungan'] = $this->input->post('status_tabungan');
                $data['jumlah_jamaah'] = $this->input->post('jumlah_jamaah');
                $data['target_saldo'] = $this->input->post('target_saldo');
                $data['sisa_target_saldo'] = $this->input->post('sisa_target_saldo');
                $data['setoran_awal'] = $this->input->post('setoran_awal');
                $data['sisa_setoran_awal'] = $this->input->post('sisa_setoran_awal');
                $data['setoran_perbulan'] = $this->input->post('setoran_perbulan');
                $data['deadline_setoran_awal'] = $deadline_setoran;
                $data['url_ktp'] = $url_ktp;
                $data['kode_reg'] = auto_code('TU-'.date('Ymd'),'');
                $data['kode_pembayaran'] = auto_code('TABRUR-INVOICE'.date('Ymd'),'');

                $result = $this->tabrur->registrasiTabrur($data);

                if ($result['status']!='failed') {

                    $this->response([
                        'status'=>$this->ok,
                        'message'=>$result['message'],
                        'data'=>$result['data']], REST_Controller::HTTP_OK);
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>$result['message'],
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }
            }else {

                $this->response([
                    'status'=>$this->error,
                    'message'=>'Data customer harap diisi',
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

            }else {

                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {

            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function tabunganCustomer_post()
    {

        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=FALSE && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents('php://input')), true);

                if ($data['fk_id_user']!='' || $data['fk_id_user']!=NULL) {

                    $result = $this->tabrur->getTabunganCustomer($data);

                    if ($result['status']!='failed') {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);
                    }else {
                        $this->response([
                            'status'=>$this->error,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }else {

                    $this->response([
                        'status'=>$this->error,
                        'message'=>'Data parameter tidak ditemukan',
                        'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function setoranAwal_post()
    {
        $user = $this->input->post('id_user');
        $url_bukti = '';

        if ($user != '' || $user !=NULL) {

            if (isset($_FILES['url_bukti']['name'])) {
                list($width, $height) = getimagesize($_FILES['url_bukti']['tmp_name']);
                $config['upload_path'] = 'files/tabrur/setoran_awal/'; //path folder file upload
                $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
                $config['max_size'] = '2000';
                $config['file_name'] = "setoran_awal_" . date('ymdhis'); //enkripsi file name upload
                $this->load->library('upload');
                $this->upload->initialize($config);
                if ($this->upload->do_upload('url_bukti')) {
                    $file_foto = $this->upload->data();
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './files/tabrur/setoran_awal/' . $file_foto['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['quality'] = '50%';
                    $config['width'] = round($width / 2);
                    $config['height'] = round($height / 2);
                    $config['new_image'] = './files/tabrur/setoran_awal/' . $file_foto['file_name'];
                    $this->load->library('image_lib');
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                    $nama_foto = 'files/tabrur/setoran_awal/' . $file_foto['file_name'];
                    $url_bukti = $nama_foto;
                }
            }

            $data['id_user'] = $this->input->post('id_user');
            $data['id_tabura'] = $this->input->post('id_tabura');
            $data['nominal'] = $this->input->post('nominal');
            $data['mekanisme'] = $this->input->post('mekanisme');
            $data['catatan'] = $this->input->post('catatan');
            $data['tanggal_pembayaran'] = $this->input->post('tanggal_pembayaran');
            $data['url_bukti'] = $url_bukti;

            $result = $this->tabrur->setoranAwal($data);

            if ($result['status']!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }


        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Data parameter kosong, harap lengkapi data anda',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function pembayaran_post()
    {
        $url_bukti = '';
        if (isset($_FILES['url_bukti']['name'])) {
            list($width, $height) = getimagesize($_FILES['url_bukti']['tmp_name']);
            $config['upload_path'] = 'files/pembayaran/'; //path folder file upload
            $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
            $config['max_size'] = '2000';
            $config['file_name'] = "konfirmasi_pembayaran_" . date('ymdhis'); //enkripsi file name upload
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('url_bukti')) {
                $file_foto = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = './files/pembayaran/' . $file_foto['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '50%';
                $config['width'] = round($width / 2);
                $config['height'] = round($height / 2);
                $config['new_image'] = './files/pembayaran/' . $file_foto['file_name'];
                $this->load->library('image_lib');
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $nama_foto = 'files/pembayaran/' . $file_foto['file_name'];
                $url_bukti = $nama_foto;
            }
        }

        $data['url_bukti'] = $url_bukti;
        $data['nomor_invoice'] = $this->input->post('nomor_invoice');
        $data['jenis_pembayaran'] = $this->input->post('jenis_pembayaran');
        $data['tanggal_transfer'] = $this->input->post('tanggal_transfer');
        $data['bank_tujuan'] = $this->input->post('bank_tujuan');
        $data['jumlah_dana'] = $this->input->post('jumlah_dana');
        $data['nama_pengirim'] = $this->input->post('nama_pengirim');
        $data['email_jamaah'] = $this->input->post('email_jamaah');
        $data['no_telpon_jamaah'] = $this->input->post('no_telpon_jamaah');
        $data['status'] = '01';

        $result = $this->tabrur->konfirmasiPembayaran($data);

        if ($result['status']!='failed') {

            $this->response([
                'status'=>$this->ok,
                'message'=>$result['message'],
                'data'=>$result['data']], REST_Controller::HTTP_OK);

        }else {
            $this->response([
            'status'=>$this->bad,
            'message'=>$result['message'],
            'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function cekPembayaran_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')), true);

        if ($data['kode_pembayaran']!='' || $data['kode_pembayaran']!=NULL) {

            $result = $this->tabrur->cekPembayaran($data);

            if ($result['status']!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>$result['data']], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }else {

            $this->response([
                'status'=>$this->bad,
                'message'=>'Parameter tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    // get /master always disabled
    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }




}
