<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Format.php';

use Restserver\Libraries\REST_Controller;


class Master extends REST_Controller{

    private $ok = '200';
    private $bad = '400';
    private $unauthorized = '401';
    private $notfound = '404';
    private $error = '500';

    function __construct($config = 'rest') {

        parent::__construct($config);
        $this->methods['data_post']['limit'] = 100; // 100 requests per hour per data/key
        $this->load->model('api/Api_master', 'master');
    }

    public function hotel_get() {

        $get = $this->master->hotel_data();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function maskapai_get() {
        $get = $this->master->maskapai_data();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }


    public function bandara_get() {
        $get = $this->master->bandara_data();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function biayatambahan_get() {
        $get = $this->master->biaya_tambahan();
        if (is_array($get) && $get != null) {
            if ($get['status'] == 'ok') {
                $result = $get['data'];

                $this->response([
                    'status' => $this->ok,
                    'data' => $result
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => $this->notfound,
                    'data' => $get['message']
                        ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response([
                'status' => $this->notfound,
                'data' => 'Data tidak ditemukan'
                    ], REST_Controller::HTTP_NOT_FOUND);
        }
    }


    public function wilayah_get()
    {
        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!=false && property_exists($decoded_token, 'id_user')) {

                $wilayah = $this->master->getWilayah();
                if (!empty($wilayah)) {

                    $this->response([
                        'status'=>$this->ok,
                        'message'=>$wilayah['message'],
                        'data'=>$wilayah['data']], REST_Controller::HTTP_OK);
                }else {
                    $this->response([
                        'status'=>$this->bad,
                        'message'=>'Data referensi wilayah tidak tersedia',
                        'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {
            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function kabupaten_post()
    {
        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= false && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents("php://input")), true);
                if (!empty($data)) {

                    $result = $this->master->getKabupatenByProvinsi($data);

                    if (!empty($result) && $result!=null) {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);

                    }else {
                        $this->response([
                            'status'=>$this->bad,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
                    }
                }else {

                    $this->response([
                        'status'=>$this->bad,
                        'message'=>'Parameter tidak boleh kosong',
                        'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {

            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function kecamatan_post()
    {

        $headers = $this->input->request_headers();

      $headers = array_change_key_case($headers,CASE_LOWER);

      if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {

        $token = $headers['authorization'];
            $decoded_token = AUTHORIZATION::validateToken($token);

            if ($decoded_token!= false && property_exists($decoded_token, "id_user")) {

                $data = json_decode(trim(file_get_contents("php://input")), true);
                if (!empty($data)) {

                    $result = $this->master->getKecamatanByKabupaten($data);
                    if (!empty($result) && $result!=null) {

                        $this->response([
                            'status'=>$this->ok,
                            'message'=>$result['message'],
                            'data'=>$result['data']], REST_Controller::HTTP_OK);

                    }else {
                        $this->response([
                            'status'=>$this->bad,
                            'message'=>$result['message'],
                            'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
                    }
                }else {

                    $this->response([
                        'status'=>$this->bad,
                        'message'=>'Parameter tidak boleh kosong',
                        'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
                }

            }else {
                $this->response([
                    'status' => $this->unauthorized,
                    'message' => 'Unathorized/Invalid Token',
                    'data'=>''], REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else {

            $this->response([
                'status' => $this->bad,
                'message' => 'Token tidak ditemukan.',
                'data'=>''], REST_Controller::HTTP_BAD_REQUEST);
        }

    }

    public function kantorcabang_get()
    {
        $result = $this->master->getKantorCabang();

        if ($result['status'] != 'failed') {

            $this->response([
                'status'=>$this->ok,
                'message'=>$result['message'],
                'data'=>$result['data']], REST_Controller::HTTP_OK);
        }else {

            $this->response([
                'status'=>$this->error,
                'message'=>$result['message'],
                'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function saran_post()
    {
        $data = json_decode(trim(file_get_contents("php://input")), true);

        if ($data['nomor_hp']!='' || $data['email']!='') {

            $saran['nama'] = $data['nama'];
            $saran['email'] = $data['email'];
            $saran['nomor_hp'] = $data['nomor_hp'];
            $saran['pesan'] = $data['pesan'];

            $result = $this->master->kirimSaran($saran);

            if ($result['status']!='failed') {

                $this->response([
                    'status'=>$this->ok,
                    'message'=>$result['message'],
                    'data'=>'1'], REST_Controller::HTTP_OK);

            }else {
                $this->response([
                    'status'=>$this->error,
                    'message'=>$result['message'],
                    'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }

        }else {
            $this->response([
                'status'=>$this->error,
                'message'=>'Nomor HP/Email tidak boleh kosong',
                'data'=>''], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // get /master always disabled
    public function index_get() {
        $this->response([
            'status' => $this->bad,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

}
