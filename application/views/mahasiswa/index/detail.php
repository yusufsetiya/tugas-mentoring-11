<?php $this->load->view('template/template_scripts') ?>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>

        </div>
    </section>
    <?php if ($this->session->flashdata('error')) : ?>
        <div class="alert alert-danger" role="alert">
            Data mahasiswa berhasil disimpan
        </div>
    <?php elseif ($this->session->flashdata('success')) : ?>
        <div class="alert alert-success" role="alert">
            Data mahasiswa Berhasil disimpan
        </div>
    <?php endif; ?>
    <div class="section-body mt-3">
        <div class="card">
            <div class="card-body">
                <form action="<?= base_url('index.php/mahasiswa/index/do_update/') . $mahasiswa['idMhs']; ?>" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukan Nama" value="<?= $mahasiswa['namaMhs']; ?>" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nim">NIM</label>
                                <input type="text" name="nim" class="form-control" value="<?= $mahasiswa['nimMhs']; ?>" id="nim" placeholder="Masukan NIM anda">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Jenis Kelamin</label>
                                <select class="form-control" name="jenkel" style="width: 100%;">
                                    <option>Pilih</option>
                                    <option <?= ($mahasiswa['jenkel'] == "L") ? 'selected' : '' ?> value="L">Laki - laki</option>
                                    <option <?= ($mahasiswa['jenkel'] == "P") ? 'selected' : '' ?> value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nim">Alamat</label>
                                <input type="text" name="alamat" value="<?= $mahasiswa['alamatMhs']; ?>" class="form-control" id="nim" placeholder="Masukan alamat anda">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Pilih Hobi</label>
                                <select name="hobi[]" class="select2" multiple="multiple" data-placeholder="Pilih hobi" style="width: 100%;">
                                    <?php foreach ($hobiAll as $item) : ?>
                                        <?php
                                        $selected = in_array($item['id'], explode(',', $mahasiswa['hobi'])) ? 'selected' : '';
                                        ?>

                                        <option value="<?= $item['id'] ?>" <?= $selected ?>><?= $item['hobi'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12  text-right">
                            <button type="submit" class="btn btn-success">Simpan Data</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}

?>