<?php $this->load->view('template/template_scripts') ?>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>

        </div>
    </section>
    <?php if ($this->session->flashdata('error')) : ?>
        <div class="alert alert-danger" role="alert">
            <?= $this->session->flashdata('success'); ?>
        </div>
    <?php elseif ($this->session->flashdata('success')) : ?>
        <div class="alert alert-success" role="alert">
            <?= $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <div class="section-body mt-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fas fa-plus"></i> Tambah Data
                        </button>
                    </div>
                </div>
                <table id="tabelMahasiswa" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 10px;">No</th>
                            <th class="text-center" style="width: 190px;">Nama</th>
                            <th class="text-center" style="width: 115px;">NIM</th>
                            <th class="text-center" style="width: 50px;">Jenis Kelamin</th>
                            <th class="text-center" style="width: 300px;">Hobi</th>
                            <th class="text-center" style="width: 130px;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($mahasiswa as $key => $value) { ?>
                            <tr>
                                <td class="text-left"><?php echo $key + 1; ?></td>
                                <td class="text-left"><?php echo $value['namaMhs']; ?></td>
                                <td class="text-left"><?php echo $value['nimMhs']; ?></td>
                                <td class="text-center"><?php echo $value['jenkel']; ?></td>
                                <td class="text-left">
                                    <?php foreach (explode(',', $value['namaHobi']) as $hobi) : ?>
                                        <span class="badge badge-success">
                                            <?php echo $hobi; ?>
                                        </span>
                                    <?php endforeach; ?>
                                </td>
                                <td class="text-center">
                                    <a href="<?= base_url('index.php/mahasiswa/index/viewUpdate/') . $value['idMhs'] ?>" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                    <a href="<?= base_url('index.php/mahasiswa/index/delete/') . $value['idMhs'] ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Mahasiswa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('index.php/mahasiswa/index/do_save') ?>" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukan Nama" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nim">NIM</label>
                                <input type="text" name="nim" class="form-control" id="nim" placeholder="Masukan NIM anda">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">Jenis Kelamin</label>
                                <select class="form-control" name="jenkel" style="width: 100%;">
                                    <option>Pilih</option>
                                    <option value="L">Laki - laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nim">Alamat</label>
                                <input type="text" name="alamat" class="form-control" id="nim" placeholder="Masukan alamat anda">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Pilih Hobi</label>
                                <select class="select2" name="hobi[]" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">
                                    <?php foreach ($hobiAll as $hobis) : ?>
                                        <option value="<?= $hobis['id']; ?>"><?= $hobis['hobi']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function() {

        $('#tabelMahasiswa').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'pdf',
                    text: 'Export PDF',
                    className: 'btn btn-secondary',
                    title: 'Data Produk',
                    spacing: '10',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
                {
                    extend: 'excel',
                    text: 'Export Excel',
                    className: 'btn btn-secondary',
                    spacing: '10',
                    title: 'Data Produk',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
                //button tambahan
                {
                    extend: 'print',
                    text: 'Print',
                    className: 'btn btn-secondary',
                    spacing: '10',
                    title: 'Data Produk',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
                {
                    extend: 'copy',
                    text: 'Copy',
                    className: 'btn btn-secondary',
                    spacing: '10',
                    title: 'Data Produk',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                }
            ]
        });
    });
</script>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}

?>