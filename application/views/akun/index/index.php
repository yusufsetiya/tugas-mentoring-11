<?php $this->load->view('template/template_scripts') ?>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>
      
    </div>
  </section>
    <div class="section-body mt-3">
      <div class="card">
        <div class="card-body">
            <?php echo $output->output; ?>
        </div>
      </div>
  </div>
</div>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}

?>