<?php $this->load->view('template/template_scripts') ?>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>
    </div>
  </section>
  <div class="section-body mt-3">
    <div class="card">
      <div class="card-body">
        <table id="tabelKategori" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Kategori</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($produk as $key => $value) { ?>
              <tr>
                <td><?php echo $key + 1; ?></td>
                <td><?php echo $value->nama; ?></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {

    //tampilkan datatable beserta semua pilihan tombol diatasnya
    $('#tabelKategori').DataTable({
      dom: 'Bfrtip',
      buttons: [{
          extend: 'pdf',
          text: 'Export PDF',
          className: 'btn btn-secondary',
          title: 'Data Produk',
          spacing: '10',
          exportOptions: {
            columns: [0, 1]
          }
        },
        {
          extend: 'excel',
          text: 'Export Excel',
          className: 'btn btn-secondary',
          spacing: '10',
          title: 'Data Produk',
          exportOptions: {
            columns: [0, 1]
          }
        },
        //button tambahan
        {
          extend: 'print',
          text: 'Print',
          className: 'btn btn-secondary',
          spacing: '10',
          title: 'Data Produk',
          exportOptions: {
            columns: [0, 1]
          }
        },
        {
          extend: 'copy',
          text: 'Copy',
          className: 'btn btn-secondary',
          spacing: '10',
          title: 'Data Produk',
          exportOptions: {
            columns: [0, 1]
          }
        }
      ]
    });
  });
</script>
<?php
if (isset($output->js_files)) {
  foreach ($output->js_files as $file) {
    echo '<script src="' . $file . '"></script>';
  }
}

?>