<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $this->config->item('webname') ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/fontawesome-free/css/all.min.css") ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url('public/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/icheck-bootstrap/icheck-bootstrap.min.css") ?>">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/jqvmap/jqvmap.min.css") ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("public/dist/css/adminlte.min.css") ?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/overlayScrollbars/css/OverlayScrollbars.min.css") ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/daterangepicker/daterangepicker.css") ?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/summernote/summernote-bs4.min.css") ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css") ?>">
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css") ?>">
  <link rel="stylesheet" href="<?php echo base_url("public/plugins/datatables-buttons/css/buttons.bootstrap4.min.css") ?>">
  <!-- Select2 -->
  <link rel="stylesheet" href=" <?php echo base_url("public/plugins/select2/css/select2.min.css") ?>">
  <link rel="stylesheet" href=" <?php echo base_url("public/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css") ?> ">
</head>

<?php
if (isset($output->css_files)) {
  foreach ($output->css_files as $file) {
    echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
  }
}
?>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
      <img class="animation__shake" src="<?php echo base_url("public/dist/img/AdminLTELogo.png") ?>" alt="AdminLTELogo" height="60" width="60">
    </div>

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
          <a class="nav-link" data-widget="navbar-search" href="#" role="button">
            <i class="fas fa-search"></i>
          </a>
          <div class="navbar-search-block">
            <form class="form-inline">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                  </button>
                  <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-controlsidebar-slide="true" href="#" role="button">
            <i class="fas fa-th-large"></i>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
        <img src="<?php echo base_url("public/dist/img/avatar5.png") ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-4" style="opacity: .8">
        <span class="brand-text font-weight-light"><?php echo $this->config->item('webname') ?></span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="<?php echo site_url('index'); ?>" class="nav-link">
                <i class="nav-icon fas fa-home"></i>
                <p>
                  Beranda
                </p>
              </a>
            </li>
            <?php if (in_array("mahasiswa.access", $userMenus)) : ?>
              <li class="nav-item">
                <a href="<?php echo site_url('mahasiswa/index') ?>" class="nav-link">
                  <i class="nav-icon fas fa-user"></i>
                  <p>
                    Mahasiswa
                  </p>
                </a>
              </li>
            <?php endif; ?>
            <?php if (in_array("akun.access", $userMenus)) : ?>
              <li class="nav-item">
                <a href="<?php echo site_url('akun/index') ?>" class="nav-link">
                  <i class="nav-icon fas fa-list"></i>
                  <p>
                    Akun
                  </p>
                </a>
              </li>
            <?php endif; ?>
            <?php if (in_array("lembaga.access", $userMenus)) : ?>
              <li class="nav-item">
                <a href="<?php echo site_url('lembaga/index') ?>" class="nav-link">
                  <i class="nav-icon fas fa-image"></i>
                  <p>
                    Lembaga
                  </p>
                </a>
              </li>
            <?php endif; ?>
            <?php if (in_array("devisi.access", $userMenus)) : ?>
              <li class="nav-item">
                <a href="<?php echo site_url('devisi/index') ?>" class="nav-link">
                  <i class="nav-icon fas fa-tags"></i>
                  <p>
                    Devisi
                  </p>
                </a>
              </li>
            <?php endif; ?>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-box"></i>
                <p>
                  Data Produk
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <?php if (in_array("product.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('produk/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Produk</p>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("categories.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('kategori/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Kategori</p>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("kantor_cabang.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('kantor_cabang/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Cabang</p>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>
                  Artikel
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo site_url('kategori_artikel/index'); ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Kategori Artikel</p>
                  </a>
                </li>
                <?php if (in_array("artikel.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('artikel/index'); ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Blog Artikel</p>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("slider.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('slider/index'); ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Slider Highlight</p>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                  Data Pokok
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <?php if (in_array("agen.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('agen/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Agen</p>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("pegawai.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('pegawai/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Pegawai</p>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("kantor_cabang.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('kantor_cabang/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Cabang</p>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Jama'ah
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <?php if (in_array("jamaah.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('jamaah/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Data jamaah</p>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("jamaah_birthday.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('jamaah_birthday/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Ulang Tahun Jamaah</p>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-gem"></i>
                <p>
                  Tabungan
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo site_url('tabrur/index') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Tabrur</p>
                  </a>
                </li>
                <?php if (in_array("tabah.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('tabah/index') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Tabah</p>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-calendar"></i>
                <p>
                  Rekap
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo site_url('rekap/index/kota') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Berdasarkan Kota</p>
                  </a>
                </li>
                <?php if (in_array("rekap.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('rekap/index/marketing') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Berdasarkan Marketing</p>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if (in_array("rekap.access", $userMenus)) : ?>
                  <li class="nav-item">
                    <a href="<?php echo site_url('rekap/index/seat') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Berdasarkan Seat</p>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </li>
            <li class="nav-item">
              <a href="<?php echo site_url('index/logout'); ?>" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                  Logout
                </p>
              </a>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper p-3">
      <div class="container-fluid">

        {CONTENT}

      </div>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
      All rights reserved.
      <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.2.0
      </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url("public/plugins/bootstrap/js/bootstrap.bundle.min.js") ?>"></script>
  <!-- ChartJS -->
  <script src="<?php echo base_url("public/plugins/chart.js/Chart.min.js") ?>"></script>
  <!-- Sparkline -->
  <script src="<?php echo base_url("public/plugins/sparklines/sparkline.js") ?>"></script>
  <!-- JQVMap -->
  <script src="<?php echo base_url("public/plugins/jqvmap/jquery.vmap.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/jqvmap/maps/jquery.vmap.usa.js") ?>"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?php echo base_url("public/plugins/jquery-knob/jquery.knob.min.js") ?>"></script>
  <!-- daterangepicker -->
  <script src="<?php echo base_url("public/plugins/moment/moment.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/daterangepicker/daterangepicker.js") ?>"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?php echo base_url("public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js") ?>"></script>
  <!-- Summernote -->
  <script src="<?php echo base_url("public/plugins/summernote/summernote-bs4.min.js") ?>"></script>
  <!-- overlayScrollbars -->
  <script src="<?php echo base_url("public/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js") ?>"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url("public/dist/js/adminlte.js") ?>"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?php echo base_url("public/dist/js/pages/dashboard.js") ?>"></script>
  <!-- DataTables  & Plugins -->
  <script src="<?php echo base_url("public/plugins/datatables/jquery.dataTables.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/datatables-responsive/js/dataTables.responsive.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/datatables-buttons/js/dataTables.buttons.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/datatables-buttons/js/buttons.bootstrap4.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/jszip/jszip.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/pdfmake/pdfmake.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/pdfmake/vfs_fonts.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/datatables-buttons/js/buttons.html5.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/datatables-buttons/js/buttons.print.min.js") ?>"></script>
  <script src="<?php echo base_url("public/plugins/datatables-buttons/js/buttons.colVis.min.js") ?>"></script>
  <!-- Select2 -->
  <script src=" <?php echo base_url("public/plugins/select2/js/select2.full.min.js") ?> "></script>
</body>

</html>