<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	*
	*/
	class Keperluan{
		function kunci(){
			$key_app='rewardgasekmultimedia';
			$panjang_key=strlen($key_app);
			return array($key_app,$panjang_key);
		}
		function waktu_time(){
			$date_time=date('YmdHis');
			$panjang_time=strlen($date_time);
			return array($date_time,$panjang_time);
		}
		function ubah_ke($pass_plain){
			$key=$this->kunci();
			$date_time=$this->waktu_time();
			$pass=base64_encode($date_time[0].$pass_plain.$date_time[0]);
			$pass=str_rot13($pass);
			$pass=base64_encode($key[0].$pass.$key[0]);
			$pass=str_rot13($pass);
			return $pass;
		}
		function balik_ke($pass_enc){
			$key=$this->kunci();
			$date_time=$this->waktu_time();
			$pass=str_rot13($pass_enc);
			$pass=base64_decode($pass);
			$pass=substr($pass,$key[1]*1,(strlen($pass)*1)-($key[1]*2));
			$pass=str_rot13($pass);
			$pass=base64_decode($pass);
			$pass=substr($pass,$date_time[1],(strlen($pass)*1)-($date_time[1]*2));
			return $pass;
		}
		function konf_tgl($text){
			$bulan=array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
			$s_tgl=explode("-", $text);
			$isi=$text;
			if(count($s_tgl)==3&&$s_tgl[1]<=12&&strlen($s_tgl[0])==4&&$isi!="0000-00-00"){
				$isi=$s_tgl[2]." ".$bulan[$s_tgl[1]-1]." ".$s_tgl[0];
			}
			return $isi;
		}
		function konf_waktu($waktu){
			$s_waktu=explode(" ", $waktu);
			$hasil=$s_waktu[1]." ".$this->konf_tgl($s_waktu[0]);
			return $hasil;
		}
		function handling($text) {
	        if ($text == ""||$text == null) {
	            return "-";
	        } else {
	            return $text;
	        }
    	}
	    function pecah2($tgl){
	        $data=explode('/', $tgl);
	        return $data[0].'-'.$data[1].'-'.$data[2];
	    }
	    function pecah3($tgl){
	        $data=explode('-', $tgl);
	        return $data[0].'/'.$data[1].'/'.$data[2];
	    }
	    function konf_tgl_for_db($range_tgl){;
	        $data=explode('-', $range_tgl);
	        return array($this->pecah2($data[0]),$this->pecah2($data[1]));
	    }
	    function back_tgl_for_db($array_tgl){;
	        return array($this->pecah3($array_tgl[0]),$this->pecah3($array_tgl[1]));
	    }
	}
?>